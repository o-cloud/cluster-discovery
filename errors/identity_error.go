package errors

type IdentityError interface {
	Unwrap() error
}

type IdentityPublicationError struct {
	Error error
}

type IdentityFetchingError struct {
	PeerId string
	Error  error
}

type IdentityTechnicalError struct {
	Error error
}

func (ipe IdentityPublicationError) Unwrap() error {
	return ipe.Error
}

func (ife IdentityFetchingError) Unwrap() error {
	return ife.Error
}

func (ite IdentityTechnicalError) Unwrap() error {
	return ite.Error
}
