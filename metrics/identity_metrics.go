package metrics

import (
	"context"
	"strconv"
	"strings"

	peerdiscoveryv1alpha1 "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	"github.com/prometheus/client_golang/prometheus"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/metrics"
)

type IdentityKey struct {
	namespace string
	name      string
}

type IdentityMetricsModel struct {
	identityKey          IdentityKey
	swarmPoolNamespace   string
	swarmPoolName        string
	swarmName            string
	totalNumberPeers     float64
	totalNumberSyncPeers float64
	status               string
}

type IdentityMetrics struct {
	metrics              map[IdentityKey]IdentityMetricsModel
	client               client.Client
	peersGaugeVector     *prometheus.GaugeVec
	syncPeersGaugeVector *prometheus.GaugeVec
}

//NewIdentityMetrics is a contructor for a metrics publisher
func NewIdentityMetrics(client client.Client) *IdentityMetrics {
	m := IdentityMetrics{
		metrics: make(map[IdentityKey]IdentityMetricsModel),
		client:  client,
		peersGaugeVector: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: "cluster_discovery_controller",
				Subsystem: "identity_controller",
				Name:      "peers_number",
				Help:      "Number of known peer associated to a given identity",
			},
			[]string{
				"identity_name",
				"identity_namespace",
				"swarmpool_namespace",
				"swarmpool_name",
				"swarm_name",
				"status",
			},
		),
		syncPeersGaugeVector: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: "cluster_discovery_controller",
				Subsystem: "identity_controller",
				Name:      "sync_peers_number",
				Help:      "Number of synchronized peers (peer with a valid published identity) associated to a given identity",
			},
			[]string{
				"identity_name",
				"identity_namespace",
				"swarmpool_namespace",
				"swarmpool_name",
				"swarm_name",
				"status",
			},
		),
	}

	metrics.Registry.MustRegister(m.peersGaugeVector, m.syncPeersGaugeVector)

	return &m
}

//Publish extract metrics values from IdentityMetrics instance cache and publish them as prometheus metrics
func (imf *IdentityMetrics) Publish() {

	for key, val := range imf.metrics {
		imf.peersGaugeVector.With(prometheus.Labels{
			"identity_name":       key.name,
			"identity_namespace":  key.namespace,
			"swarmpool_namespace": val.swarmPoolNamespace,
			"swarmpool_name":      val.swarmPoolName,
			"swarm_name":          val.swarmName,
			"status":              val.status,
		}).Set(val.totalNumberPeers)

		imf.syncPeersGaugeVector.With(prometheus.Labels{
			"identity_name":       key.name,
			"identity_namespace":  key.namespace,
			"swarmpool_namespace": val.swarmPoolNamespace,
			"swarmpool_name":      val.swarmPoolName,
			"swarm_name":          val.swarmName,
			"status":              val.status,
		}).Set(val.totalNumberSyncPeers)
	}
}

//Fetch puts metrics value in IdentityMetrics instance cache
func (imf *IdentityMetrics) Fetch() error {
	// Get all the identities from api server
	var idList peerdiscoveryv1alpha1.IdentityList
	ctx := context.Background()
	newMetrics := make(map[IdentityKey]IdentityMetricsModel)

	if err := imf.client.List(ctx, &idList); err != nil {
		return err
	}

	for _, id := range idList.Items {
		// Iterate over status
		// Might be better to get the info from the underlying IPFS network instead of creating
		// a dependency with kub object status (info are fetcehd from status here)
		for key, idStatus := range id.Status.IdentitiesStatus {

			var (
				totalNumberSyncPeers float64
				totalNumberPeers     float64
			)

			peerSyncInf := strings.Split(idStatus.PeersSync, "/")
			swarmPoolInf := strings.Split(key, "/")

			val, err := strconv.Atoi(peerSyncInf[0])
			if err != nil {
				totalNumberSyncPeers = -1.0
			} else {
				totalNumberSyncPeers = float64(val)
			}

			val, err = strconv.Atoi(peerSyncInf[1])
			if err != nil {
				totalNumberPeers = -1.0
			} else {
				totalNumberPeers = float64(val)
			}

			newMetric := IdentityMetricsModel{
				identityKey:          IdentityKey{name: id.Name, namespace: id.Namespace},
				totalNumberPeers:     totalNumberPeers,
				totalNumberSyncPeers: totalNumberSyncPeers,
				status:               string(idStatus.IdentitySync),
				swarmPoolNamespace:   swarmPoolInf[0],
				swarmPoolName:        swarmPoolInf[1],
				swarmName:            swarmPoolInf[2],
			}

			newMetrics[newMetric.identityKey] = newMetric
		}
	}

	imf.metrics = newMetrics

	return nil
}
