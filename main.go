/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"flag"
	"os"
	"time"

	"k8s.io/client-go/kubernetes"

	peerdiscoveryv1alpha1 "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"
	"irt.saint-exupery.com/cluster-discovery/controllers"
	identitymetrics "irt.saint-exupery.com/cluster-discovery/metrics"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	//+kubebuilder:scaffold:imports
	// Mem tracing
	//_ "net/http/pprof"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(peerdiscoveryv1alpha1.AddToScheme(scheme))
	//+kubebuilder:scaffold:scheme

}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "cluster-discovery-le-id",
		Port:                   9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.IdentityReconciler{
		Client:              mgr.GetClient(),
		Scheme:              mgr.GetScheme(),
		RefreshStatusPeriod: 20 * time.Second,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Identity")
		os.Exit(1)
	}
	if err = (&controllers.PeerReconciler{
		Client:              mgr.GetClient(),
		Scheme:              mgr.GetScheme(),
		RefreshStatusPeriod: 5 * time.Second,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Peer")
		os.Exit(1)
	}

	clientSet := kubernetes.NewForConfigOrDie(mgr.GetConfig())
	resource := types.NamespacedName{Namespace: "cluster-discovery-default-config", Name: "default-bootstrap"}
	configMapClient := clientSet.CoreV1().ConfigMaps(resource.Namespace)
	cfgMap, err := configMapClient.Get(context.Background(), resource.Name, metav1.GetOptions{})
	if err != nil {
		setupLog.Error(err, "unable to create controller, ", "controller", "SwarmPool")
		os.Exit(1)
	}

	if err = (&controllers.SwarmPoolReconciler{
		Client:                  mgr.GetClient(),
		Scheme:                  mgr.GetScheme(),
		ClientSet:               clientSet,
		Recorder:                mgr.GetEventRecorderFor("swarmpool-controller"),
		DefaultBootstrapAddress: cfgMap.Data["default_bootstrap_address"],
		RefreshStatusPeriod:     20 * time.Second,
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "SwarmPool")
		os.Exit(1)
	}
	//+kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	/* Mem tracing
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	*/

	setupLog.Info("starting metrics fetcher")

	go func(im *identitymetrics.IdentityMetrics, period time.Duration) {
		ticker := time.NewTicker(period)
		for range ticker.C {
			err := im.Fetch()
			if err == nil {
				im.Publish()
			}
		}
	}(identitymetrics.NewIdentityMetrics(mgr.GetClient()), time.Second*10)

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}

}
