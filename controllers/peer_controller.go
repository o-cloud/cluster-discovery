/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/tls"
	"net/http"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	peerdiscoveryv1alpha1 "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"
	"irt.saint-exupery.com/cluster-discovery/util"
)

// PeerReconciler reconciles a Peer object
type PeerReconciler struct {
	client.Client
	Scheme              *runtime.Scheme
	Recorder            record.EventRecorder
	RefreshStatusPeriod time.Duration
}

//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=peers,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=peers/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=peers/finalizers,verbs=update

func (r *PeerReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	var (
		peer peerdiscoveryv1alpha1.Peer
	)

	if err := r.Get(ctx, req.NamespacedName, &peer); err != nil {
		return ctrl.Result{}, util.IgnoreNotFound(err)
	}

	/// Basic "online" status computation: if Get request on published API server doesn't fail, Peer status is considered ok
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	http.DefaultTransport.(*http.Transport).ResponseHeaderTimeout = time.Duration(5) * time.Second

	resp, err := http.Get(peer.Spec.URL)
	previousStatus := peer.Status.PeerStatus
	if err != nil {
		peer.Status.PeerStatus = peerdiscoveryv1alpha1.Offline
	} else {
		peer.Status.PeerStatus = peerdiscoveryv1alpha1.Online
	}
	if previousStatus != peer.Status.PeerStatus {
		logger.Info("Peer Availability status", "peer", req.NamespacedName)
	}
	if resp != nil {
		defer resp.Body.Close()
	}

	if err := r.Status().Update(ctx, &peer); err != nil {
		logger.Error(err, "Can't update status")
	}

	return ctrl.Result{RequeueAfter: r.RefreshStatusPeriod}, nil
}

func (r *PeerReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&peerdiscoveryv1alpha1.Peer{}).
		Complete(r)
}
