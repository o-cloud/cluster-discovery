package controllers

import (
	"context"
	"fmt"
	"net/url"

	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"
	ipfsclient "irt.saint-exupery.com/cluster-discovery/ipfs_client"
	rm "irt.saint-exupery.com/cluster-discovery/resources_manager"
	"irt.saint-exupery.com/cluster-discovery/util"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	traefikclient "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/generated/clientset/versioned"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type SwarmController struct {
	SwarmPool cd.SwarmPool
	R         client.Reader
	W         client.Writer
	SW        client.StatusWriter
	Recorder  record.EventRecorder
	// Need this as Reader from controller runtime only read from cache, not from kube API
	Client                  *kubernetes.Clientset
	L                       logr.Logger
	DefaultBootstrapAddress string
}

func (sc *SwarmController) CreateOrUpdate(ctx context.Context, swarm cd.Swarm) error {

	// Resources name generator
	rn := rm.NewResourcesNaming(sc.SwarmPool, swarm)

	// Generate owner reference for garbage collection support
	isController := true
	ownerRef := v1.OwnerReference{
		APIVersion: sc.SwarmPool.APIVersion,
		Kind:       sc.SwarmPool.Kind,
		Name:       sc.SwarmPool.Name,
		UID:        sc.SwarmPool.UID,
		Controller: &isController,
	}

	// Configuring edge exposition
	erm := rm.NewEdgeResourcesManager(ownerRef, rn, swarm.Network)

	if erm.HasLoadBalancer() && erm.HasTraefikIntegration() {
		msg := fmt.Sprintf("Can't have a traefik and Load balancer integration at the same time for %s/%s", sc.SwarmPool.Name, swarm.Name)
		sc.Recorder.Event(&sc.SwarmPool, "Warning", "Creation", msg)
		return fmt.Errorf(msg)
	}

	if erm.HasLoadBalancer() {
		var lbService corev1.Service

		if err := sc.R.Get(ctx, rn.GetLoadBalancerNamespacedName(), &lbService); err != nil {
			if apierrs.IsNotFound(err) {
				lbManifest := erm.LoadBalancerManifest()
				sc.L.Info("Creating load balancer", "id", swarm.Name)
				if err := sc.W.Create(ctx, lbManifest); err != nil {
					return err
				}
				sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Instanciate Load Balancer for swarm %s/%s", sc.SwarmPool.Name, swarm.Name))
			} else {
				return err
			}
		} else {
			// don't update if spec has not changed
			if sc.SwarmPool.HasSpecChanged() {
				sc.L.Info("Updating load balancer", "id", swarm.Name)
				lbManifest := erm.UpdateLoadBalancerManifest(&lbService, swarm)
				if err := sc.W.Update(ctx, lbManifest); err != nil {
					return err
				}
			}
		}

		// Looking up for IP
		if len(lbService.Status.LoadBalancer.Ingress) == 0 {
			sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Waiting external IP provisioning for %s/%s", sc.SwarmPool.Name, swarm.Name))
			return fmt.Errorf("no IP set to %s load balancer", rn.GetLoadBalancerNamespacedName())
		} else {
			erm.SetIP(lbService.Status.LoadBalancer.Ingress[0].IP)
		}
	}

	if erm.HasTraefikIntegration() {
		var traefikService corev1.Service

		// Instanciate traefik client
		tc, _ := traefikclient.NewForConfig(ctrl.GetConfigOrDie())

		// Handling traefik service
		if err := sc.R.Get(ctx, rn.GetTraefikTCPServiceNamespacedName(), &traefikService); err != nil {
			if apierrs.IsNotFound(err) {

				traefikServiceManifest := erm.TraefikServiceManifest()
				sc.L.Info("Creating traefik service for swarm", "id", swarm.Name)
				if err := sc.W.Create(ctx, traefikServiceManifest); err != nil {
					return err
				}
				sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Instanciate traefik headless service for swarm %s/%s", sc.SwarmPool.Name, swarm.Name))

			} else {
				return err
			}
		} else {
			// don't update if spec has not changed
			if sc.SwarmPool.HasSpecChanged() {
				sc.L.Info("Updating headless traefik service", "id", swarm.Name)

				// Service
				lbManifest := erm.UpdateTraefikServiceManifest(traefikService)
				if err := sc.W.Update(ctx, lbManifest); err != nil {
					return err
				}
			}
		}

		if traefikRoute, err := tc.TraefikV1alpha1().
			IngressRouteTCPs(rn.GetTraefikIngressRouteNamespacedName().Namespace).
			Get(ctx, rn.GetTraefikIngressRouteNamespacedName().Name, v1.GetOptions{}); err != nil {
			if apierrs.IsNotFound(err) {
				// Traefik TCP Route
				tcpRoute := erm.IngressRouteManifest()
				if _, err := tc.TraefikV1alpha1().IngressRouteTCPs(rn.GetTraefikIngressRouteNamespacedName().Namespace).Create(ctx, tcpRoute, v1.CreateOptions{}); err != nil {
					return err
				}
				sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Instanciate Traefik TCP Route for swarm %s/%s", sc.SwarmPool.Name, swarm.Name))

			} else {
				return err
			}
		} else {
			// don't update if spec has not changed
			if sc.SwarmPool.HasSpecChanged() {
				sc.L.Info("Updating traefik tcp route ", "id", swarm.Name)

				tcpRoute := erm.UpdateIngressRouteManifest(*traefikRoute)
				if _, err := tc.TraefikV1alpha1().IngressRouteTCPs(rn.GetTraefikIngressRouteNamespacedName().Namespace).Update(ctx, tcpRoute, v1.UpdateOptions{}); err != nil {
					return err
				}
			}
		}

		// Looking up for IP
		var traefikLbService corev1.Service
		lbNamespacedName := types.NamespacedName{
			Namespace: swarm.Network.Traefik.TraefikLoadBalancerNamespace,
			Name:      swarm.Network.Traefik.TraefikLoadBalancerService,
		}
		if err := sc.R.Get(ctx, lbNamespacedName, &traefikLbService); err != nil {
			return err
		}
		if len(traefikLbService.Status.LoadBalancer.Ingress) == 0 {
			sc.Recorder.Event(&sc.SwarmPool, "Warning", "Creation", fmt.Sprintf("No IP found for for traefik load balancer %s", lbNamespacedName))
			return fmt.Errorf("no IP found for for traefik load balancer %s", lbNamespacedName)
		}
		erm.SetIP(traefikLbService.Status.LoadBalancer.Ingress[0].IP)
	}

	//cluster internal IP service
	// Service exists if a traefik integration is used, and if IPFS API is not exposed with LoadBalancer
	var cip corev1.Service
	if !swarm.Network.LoadBalancer.ExposedAPI {
		ism := rm.NewClusterIPResourcesManager(ownerRef, rn, swarm)
		cipManifest := ism.ClusterIPManifest()

		if err := sc.R.Get(ctx, rn.GetIPFSAPIServiceNamespacedName(), &cip); err != nil {
			if apierrs.IsNotFound(err) {
				sc.L.Info("Creating service", "id", swarm.Name)
				if err := sc.W.Create(ctx, cipManifest); err != nil {
					return err
				}
				sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Instanciate headless service for swarm %s/%s", sc.SwarmPool.Name, swarm.Name))
			} else {
				return err
			}
		} else {
			cipManifest := ism.UpdateClusterIPManifest(cip)
			if err := sc.W.Update(ctx, cipManifest); err != nil {
				sc.L.Info("Updating service error", "id", swarm.Name)
				return err
			}
		}
	}

	// // Configuring stateful set
	var ss appsv1.StatefulSet
	ssrm := rm.NewStatefulSetResourcesManager(ownerRef, rn, swarm, erm.GetIP(), sc.DefaultBootstrapAddress)
	ssManifest, err := ssrm.StatefulSetManifest()
	if err != nil {
		return err
	}

	if err := sc.R.Get(ctx, rn.GetStatefulSetNamespacedName(), &ss); err != nil {
		if apierrs.IsNotFound(err) {
			sc.L.Info("Creating stateful set", "id", swarm.Name)
			if err := sc.W.Create(ctx, ssManifest); err != nil {
				return err
			}
			sc.Recorder.Event(&sc.SwarmPool, "Normal", "Creation", fmt.Sprintf("Instanciate stateful set service for swarm %s", sc.SwarmPool.Name))
		} else {
			return err
		}
	} else {
		ssManifest := ssrm.UpdateStatefulSetManifest(&ss)
		if err := sc.W.Update(ctx, ssManifest); err != nil {
			return err
		}
	}

	return nil
}

func (sc *SwarmController) DeleteSwarm(ctx context.Context, swarmPool types.NamespacedName, swarm cd.Swarm) error {
	rn := rm.NewResourcesNaming(sc.SwarmPool, swarm)
	isController := true
	ownerRef := v1.OwnerReference{
		APIVersion: sc.SwarmPool.APIVersion,
		Kind:       sc.SwarmPool.Kind,
		Name:       sc.SwarmPool.Name,
		UID:        sc.SwarmPool.UID,
		Controller: &isController,
	}

	var lb corev1.Service
	var traefikSvc corev1.Service
	var apiSvc corev1.Service
	var ss appsv1.StatefulSet

	// Configuring edge exposition
	erm := rm.NewEdgeResourcesManager(ownerRef, rn, swarm.Network)

	if erm.HasLoadBalancer() {
		// Handling load balancer deletion
		if err := sc.R.Get(ctx, rn.GetLoadBalancerNamespacedName(), &lb); err != nil {
			if !apierrs.IsNotFound(err) {
				return err
			}
		} else {
			sc.L.Info("Deleting load balancer", "id", swarmPool.Namespace+"/"+swarmPool.Name+"/"+swarm.Name)
			if err := sc.W.Delete(ctx, &lb); err != nil {
				if !apierrs.IsNotFound(err) {
					return err
				}
			}
		}
	}

	if erm.HasTraefikIntegration() {
		// Handling traefik headless service deletion
		if err := sc.R.Get(ctx, rn.GetTraefikTCPServiceNamespacedName(), &traefikSvc); err != nil {
			if !apierrs.IsNotFound(err) {
				return err
			}
		} else {
			sc.L.Info("Deleting traefik headless service ", "id", swarmPool.Namespace+"/"+swarmPool.Name+"/"+swarm.Name)
			if err := sc.W.Delete(ctx, &lb); err != nil {
				if !apierrs.IsNotFound(err) {
					return err
				}
			}
		}
	}

	// Handling headless service deletion
	if err := sc.R.Get(ctx, rn.GetIPFSAPIServiceNamespacedName(), &apiSvc); err != nil {
		if !apierrs.IsNotFound(err) {
			return err
		}
	} else {
		sc.L.Info("Deleting service", "id", swarmPool.Namespace+"/"+swarmPool.Name+"/"+swarm.Name)
		if err := sc.W.Delete(ctx, &apiSvc); err != nil {
			if !apierrs.IsNotFound(err) {
				return err
			}
		}
	}

	// Handling stateful set deletion
	if err := sc.R.Get(ctx, rn.GetStatefulSetNamespacedName(), &ss); err != nil {
		if !apierrs.IsNotFound(err) {
			return err
		}
	} else {
		sc.L.Info("Deleting stateful set", "id", swarmPool.Namespace+"/"+swarmPool.Name+"/"+swarm.Name)
		if err := sc.W.Delete(ctx, &ss); err != nil {
			if !apierrs.IsNotFound(err) {
				return err
			}
		}
	}

	sc.Recorder.Event(&sc.SwarmPool, "Normal", "Deletion", fmt.Sprintf("%s swarm client has been deleted", swarm.Name))

	return nil
}

func (sc *SwarmController) FindSwarms(context context.Context, swarmPool cd.SwarmPool) ([]cd.Swarm, error) {

	swarmNames := make(map[string]bool)
	swarms := []cd.Swarm{}
	svcClient := sc.Client.CoreV1().Services(swarmPool.Namespace)
	ssClient := sc.Client.AppsV1().StatefulSets(swarmPool.Namespace)

	// Consider a swarm exists if either a service or stateful set exists for the (swarmPoolName, swarmName) couple
	svcs, err := svcClient.List(context, v1.ListOptions{LabelSelector: "swarmPool"})
	if err != nil {
		return []cd.Swarm{}, err
	}

	sss, err := ssClient.List(context, v1.ListOptions{LabelSelector: "swarmPool"})
	if err != nil {
		return []cd.Swarm{}, err
	}

	for _, svc := range svcs.Items {
		if val, ok := svc.ObjectMeta.Labels["swarmPool"]; ok {
			if val == swarmPool.Name {
				swarmNames[svc.ObjectMeta.Labels["swarm"]] = true
			}
		}
	}

	for _, ss := range sss.Items {
		if val, ok := ss.ObjectMeta.Labels["swarmPool"]; ok {
			if val == swarmPool.Name {
				swarmNames[ss.ObjectMeta.Labels["swarm"]] = true
			}
		}
	}

	// swarms contains the name of the existing swarm; retrieve the Swarm objects from the swarmPool
	for swarmName := range swarmNames {
		// Retrieve swarms with given name in swarmPool
		for i := 0; i < len(swarmPool.Spec.Swarms); i++ {
			if swarmName == swarmPool.Spec.Swarms[i].Name {
				swarms = append(swarms, swarmPool.Spec.Swarms[i])
			}
		}
	}

	return swarms, nil
}

// Status fetches informations through IFPS API server, and returns a status
func (sc *SwarmController) Status(ctx context.Context, swarm cd.Swarm) (*cd.SwarmStatus, error) {
	var url *url.URL
	var err error

	// Request API through service (might be the load balancer service or clusterIP service depending of the configuration)
	url, err = util.IPFSServiceURL(ctx, sc.R, sc.SwarmPool, swarm)

	if err != nil {
		return nil, err
	}

	c, err := ipfsclient.NewIPFSClient(*url)
	if err != nil {
		return nil, err
	}

	bootstrapPeers, errBootstrap := c.GetBootstrapPeers(ctx)
	if errBootstrap != nil {
		return nil, errBootstrap
	}

	peers, errPeers := c.ListPeersAddress(ctx)
	if errPeers != nil {
		return nil, errPeers
	}

	id, addresses, errID := c.ID()
	if errID != nil {
		return nil, errID
	}

	status := cd.SwarmStatus{
		Name:                 swarm.Name,
		ID:                   id,
		Addresses:            addresses,
		NbConnectedPeers:     len(*peers),
		NbBootstrapAddresses: len(bootstrapPeers),
	}

	return &status, nil
}
