/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"strings"
	"time"

	clusterdisc "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/record"

	"irt.saint-exupery.com/cluster-discovery/util"

	corev1 "k8s.io/api/core/v1"

	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	apierrs "k8s.io/apimachinery/pkg/api/errors"
)

// SwarmPoolReconciler reconciles a SwarmPool object
type SwarmPoolReconciler struct {
	client.Client
	Scheme                  *runtime.Scheme
	ClientSet               *kubernetes.Clientset
	Recorder                record.EventRecorder
	RefreshStatusPeriod     time.Duration
	DefaultBootstrapAddress string
}

//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=swarmpools,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=swarmpools/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=swarmpools/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;update;create;list;delete;watch
//+kubebuilder:rbac:groups="",resources=services,verbs=get;update;create;list;delete;watch
//+kubebuilder:rbac:groups="",resources=events,verbs=get;create;list;delete;patch
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;create;delete;list;watch
//+kubebuilder:rbac:groups="",resources=configmaps,verbs=get;create;list
//+kubebuilder:rbac:groups=traefik.containo.us,resources=ingressroutetcps,verbs=get;list;create;update;patch;delete;watch

func (r *SwarmPoolReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var (
		swarmPool  clusterdisc.SwarmPool
		lastUpdate time.Time
	)

	logger := log.FromContext(ctx).WithName(req.Name)
	errs := []error{}
	status := []clusterdisc.SwarmStatus{}

	// Retrieve object
	if err := r.Get(ctx, req.NamespacedName, &swarmPool); err != nil {
		logger.Info("Swarmpool does not exists anymore")
		return util.ResultRequeueOnError(util.IgnoreNotFound(err))
	}

	// Instanciate a swarm controller to handle all the business logic
	sc := SwarmController{
		Client:                  r.ClientSet,
		SwarmPool:               swarmPool,
		R:                       r,
		W:                       r,
		SW:                      r.Status(),
		Recorder:                r.Recorder,
		L:                       logger,
		DefaultBootstrapAddress: r.DefaultBootstrapAddress,
	}

	// Find existing deployed swarms, eventually
	swarms, err := sc.FindSwarms(ctx, swarmPool)
	if err != nil {
		logger.Info("Couldn't find swarms")
		return util.ResultRequeueOnError(err)
	}

	// Delete the one which are not supported anymore
	for _, existingSwarm := range swarms {
		toDelete := true
		for _, targetedSwarm := range swarmPool.Spec.Swarms {
			if existingSwarm.Name == targetedSwarm.Name {
				toDelete = false
				break
			}
		}
		if toDelete {
			logger.Info("Deleting swarm", "id", req.NamespacedName)
			if err := sc.DeleteSwarm(ctx, req.NamespacedName, existingSwarm); err != nil {
				logger.Info("Error deleting swarm", "id", req.NamespacedName)
				errs = append(errs, err)
				continue
			}
		}
	}

	// Check if each pool has been deployed
	for _, swarm := range swarmPool.Spec.Swarms {
		// 3 cases:
		// Swarm key, bootstrap address: connect to existing swarm
		// Swarm key, no bootstrap address: custom swarm initiates by the cluster: generate the swarm key value
		// No swarm key, no bootstrap address: connect to default swarm
		if swarm.SwarmDefinition.SwarmKeySecretRef != "" && len(swarm.SwarmDefinition.BootstrapNodeAddresses) == 0 {
			sNN := types.NamespacedName{Namespace: swarmPool.Namespace, Name: swarm.SwarmDefinition.SwarmKeySecretRef}
			hasSwarm, err := r.hasSwarmKey(ctx, sNN)
			if err != nil {
				errs = append(errs, err)
				logger.Info("Error checking swarmKey", "swarm", swarm.Name)
				continue
			}
			if !hasSwarm {
				logger.Info("Creating swarm key ", "swarmPool", swarmPool.Namespace+"/"+swarmPool.Name+"/"+swarm.Name)
				if err := r.createSwarmKey(ctx, sNN, swarmPool); err != nil {
					errs = append(errs, err)
					logger.Info("Error creating swarmKey", "swarm", swarm.Name)
					continue
				}

				//Adding annotation specifying that the swarmpool has created its own key
				swarmPool.ObjectMeta.Annotations["self-created"] = "true"
				if err := r.Update(ctx, &swarmPool); err != nil {
					errs = append(errs, err)
					logger.Info("Error updating swarm", "swarm", swarm.Name)
					continue
				}
			}
		}

		if swarm.SwarmDefinition.SwarmKeySecretRef == "" && swarm.SwarmDefinition.BootstrapNodeAddresses == nil {
			err := r.duplicateDefaultKey(ctx, req.Namespace)
			if err != nil {
				logger.Info("Error duplicateDefaultKey", "swarm", swarm.Name)
				errs = append(errs, err)
				continue
			}
		}

		// Handle swarmpool spec requirements
		if err := sc.CreateOrUpdate(ctx, swarm); err != nil {
			logger.Info("Error updating swarm", "swarm", swarm.Name)
			errs = append(errs, err)
		}

		// Generate status
		st, err := sc.Status(ctx, swarm)
		if err == nil {
			if st != nil {
				status = append(status, *st)
			}
		} else {
			logger.Info("Error checking swarm status", "swarm", swarm.Name)
			errs = append(errs, err)
		}
	}

	// Handling status
	swarmPool.UpdateSpecHash()

	swarmPool.Status.SwarmsStatus = make(map[string]clusterdisc.SwarmStatus)

	for _, st := range status {
		swarmPool.Status.SwarmsStatus[st.Name] = st
	}

	if swarmPool.Status.LastUpdate != "" {
		lastUpdate, _ = time.Parse(time.RFC3339, swarmPool.Status.LastUpdate)
	}

	if swarmPool.Status.LastUpdate == "" || time.Since(lastUpdate) > r.RefreshStatusPeriod {
		swarmPool.Status.LastUpdate = time.Now().Format(time.RFC3339)
		if err := sc.SW.Update(ctx, &swarmPool); err != nil {
			logger.Info("Error updating swarmpool status")
			errs = append(errs, err)
		}
	}

	// err is not returned as it triggers exponential backoff retry
	if len(errs) > 0 {
		var out strings.Builder
		for _, myErr := range errs {
			out.WriteString(myErr.Error() + "\n")
		}
		sc.L.Info("Swarmpool processing Error", "id", req.NamespacedName.Name, "errors", out.String())
		// Reposting at fix rate if error occurs, bypassing the default exponential retry
		return ctrl.Result{RequeueAfter: time.Duration(10 * time.Second)}, nil
	}

	// Keep reposting to update status
	return ctrl.Result{}, nil
}

func (r *SwarmPoolReconciler) hasSwarmKey(context context.Context, nn types.NamespacedName) (bool, error) {
	secretClient := r.ClientSet.CoreV1().Secrets(nn.Namespace)
	secret, err := secretClient.Get(context, nn.Name, metav1.GetOptions{})
	if err != nil && !apierrs.IsNotFound(err) {
		return false, err
	}
	// Could do a better data value check, but it's enough for now to know that data exists
	if secret != nil && secret.Data != nil {
		if _, ok := secret.Data["swarm-key"]; !ok {
			return false, nil
		}
		return true, nil
	}

	return false, nil
}

func (r *SwarmPoolReconciler) createSwarmKey(context context.Context, nn types.NamespacedName, swarmPool clusterdisc.SwarmPool) error {
	var sc *corev1.Secret
	alreadyExists := true
	isController := true
	key := make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		return err
	}

	//Format key
	strKey := fmt.Sprintf("/key/swarm/psk/1.0.0/\n/base16/\n%s", hex.EncodeToString(key))

	secretClient := r.ClientSet.CoreV1().Secrets(nn.Namespace)

	// Eventually, retrieve specified secret
	sc, err = secretClient.Get(context, nn.Name, metav1.GetOptions{})
	if err != nil {
		if apierrs.IsNotFound(err) {
			alreadyExists = false
			sc = &corev1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      nn.Name,
					Namespace: nn.Namespace,
					OwnerReferences: []metav1.OwnerReference{
						{
							APIVersion: swarmPool.APIVersion,
							Kind:       swarmPool.Kind,
							Name:       swarmPool.Name,
							UID:        swarmPool.UID,
							Controller: &isController,
						},
					},
				},
				Data: make(map[string][]byte),
			}
		}
	}
	sc.Data["swarm-key"] = []byte(strKey)

	if alreadyExists {
		if _, err := secretClient.Update(context, sc, metav1.UpdateOptions{}); err != nil {
			return err
		}
	} else {
		if _, err := secretClient.Create(context, sc, metav1.CreateOptions{}); err != nil {
			return err
		}
	}
	return nil
}

func (r *SwarmPoolReconciler) duplicateDefaultKey(context context.Context, targetNamespace string) error {
	sourceClient := r.ClientSet.CoreV1().Secrets("cluster-discovery-default-config")
	targetClient := r.ClientSet.CoreV1().Secrets(targetNamespace)
	var destSc corev1.Secret

	// Check if already exists
	_, err := targetClient.Get(context, "default-swarm-key", metav1.GetOptions{})

	if err != nil {
		if apierrs.IsNotFound(err) {

			// Otherwise fetch reference
			refSc, err := sourceClient.Get(context, "default-swarm-key", metav1.GetOptions{})
			if err != nil {
				return err
			}

			// Modify namespace target
			refSc.DeepCopyInto(&destSc)

			destSc.ObjectMeta = metav1.ObjectMeta{
				Name:      "default-swarm-key",
				Namespace: targetNamespace,
			}

			if _, err := targetClient.Create(context, &destSc, metav1.CreateOptions{}); err != nil {
				return err
			}

		} else {
			return err
		}
	}

	return nil
}

func (r *SwarmPoolReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&clusterdisc.SwarmPool{}).
		Complete(r)
}
