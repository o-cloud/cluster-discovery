/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	clusterdisc "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"
	ipfsclient "irt.saint-exupery.com/cluster-discovery/ipfs_client"
	"irt.saint-exupery.com/cluster-discovery/util"

	"github.com/BurntSushi/toml"
	corev1 "k8s.io/api/core/v1"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
)

// IdentityReconciler reconciles a Identity object
type IdentityReconciler struct {
	client.Client
	Scheme              *runtime.Scheme
	RefreshStatusPeriod time.Duration
}

type SwarmNotFound struct {
	Reason string
}

func (snf SwarmNotFound) Error() string {
	return snf.Reason
}

//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=identities,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=identities/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=peerdiscovery.irt-saintexupery.com,resources=identities/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources=serviceaccounts,verbs=get;create;delete;list;watch
//+kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=roles,verbs=get;create;delete;list
//+kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=rolebindings,verbs=get;create;delete;list
//+kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=clusterroles,verbs=bind
//+kubebuilder:rbac:groups="",resources=namespaces,verbs=get;create;delete;update;patch;list;watch

func (r *IdentityReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	finalizerName := "identityFinalizer"
	logger := log.FromContext(ctx).WithName(req.Name)
	var (
		identity     clusterdisc.Identity
		namespace    corev1.Namespace
		swarmPool    clusterdisc.SwarmPool
		swarm        clusterdisc.Swarm
		swarmNetwork clusterdisc.SwarmNetwork
		out          strings.Builder
		err          error
		lastUpdate   time.Time
	)

	errs := []error{}

	// Get the identity object
	if err := r.Get(ctx, req.NamespacedName, &identity); err != nil {
		logger.Info("Identity not found")
		return ctrl.Result{}, util.IgnoreNotFound(err)
	}

	// if no annotations, map is nil
	if identity.Annotations == nil {
		identity.Annotations = make(map[string]string)
	}

	// Deletion ?
	if identity.ObjectMeta.DeletionTimestamp.IsZero() {
		if !util.ContainsString(identity.ObjectMeta.Finalizers, finalizerName) {
			logger.Info(fmt.Sprintf("New Identity ? Appending finalizer"))
			identity.ObjectMeta.Finalizers = append(identity.ObjectMeta.Finalizers, finalizerName)
			if err := r.Update(context.Background(), &identity); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		// Deletion !
		if util.ContainsString(identity.ObjectMeta.Finalizers, finalizerName) {
			if errs := r.deleteIdentity(ctx, &identity); len(errs) > 0 {
				shouldRetry := true

				//If swarm associated to identity is not found for whatever reason, we give up
				for _, err := range errs {
					switch err.(type) {
					case SwarmNotFound:
						shouldRetry = false
					}
					if apierrs.IsNotFound(err) {
						shouldRetry = false
					}
				}
				if shouldRetry {
					return ctrl.Result{}, errs[0]
				}
			}

			identity.ObjectMeta.Finalizers = util.RemoveString(identity.ObjectMeta.Finalizers, finalizerName)
			if err := r.Update(context.Background(), &identity); err != nil {
				logger.Error(err, "Error removing finalizer", "id", req.NamespacedName)
				return ctrl.Result{}, err
			}
		}
		// Stop reconciliation as the item is being deleted
		logger.Info("Identity deleted", "id", req.NamespacedName)
		return ctrl.Result{}, nil
	}

	// Instanciate new identities status array
	identitiesStatus := make(map[string]clusterdisc.PeersSyncAndIdentitySync)
	identity.ObjectMeta.OwnerReferences = []metav1.OwnerReference{}

	// Iterate over targeted swarms
	for _, swarmID := range identity.Spec.PublicationTargets {

		peerSyncAndIdentity := clusterdisc.PeersSyncAndIdentitySync{
			IdentitySync: clusterdisc.OutOfSync,
			PeersSync:    "?/?",
		}

		// Get the associated swarm pools
		if err = r.Get(ctx, types.NamespacedName{Namespace: swarmID.Namespace, Name: swarmID.SwarmPoolName}, &swarmPool); err != nil {
			logger.Info(fmt.Sprintf("Error retrieving swarmpool %s/%s associated with identity", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		// Retrieve targeted swarm
		swarm = clusterdisc.Swarm{}
		swarmNetwork = clusterdisc.SwarmNetwork{}

		for _, s := range swarmPool.Spec.Swarms {
			if s.Name == swarmID.Swarm {
				swarm = s
				swarmNetwork = s.Network
				break
			}
		}

		// check if swarm has been found
		if swarmNetwork.SwarmPort == 0 && swarmNetwork.APIPort == 0 {
			logger.Info(fmt.Sprintf("Invalid port info for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, fmt.Errorf("%s/%s/%s not found", swarmID.Namespace, swarmID.SwarmPoolName, swarmID.Swarm))
			continue
		}

		// Instanciate IPFS client, get headless or lb url first
		url, err := util.IPFSServiceURL(ctx, r, swarmPool, swarm)
		if err != nil {
			logger.Info(fmt.Sprintf("Error generating ipfs service url for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		// Instancie IPFS client for real
		ipfsClient, err := ipfsclient.NewIPFSClient(*url)
		if err != nil {
			logger.Info(fmt.Sprintf("Error creating ipfs client for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		// Handle remote peers creation/update/deletion
		nbPeers, nbFailedPeers, _ := r.syncPeers(ctx, identity, swarmPool, ipfsClient)
		if nbPeers != nil && nbFailedPeers != nil {
			peerSyncAndIdentity.PeersSync = fmt.Sprintf("%d/%d", *nbPeers-*nbFailedPeers, *nbPeers)
		} else {
			logger.Info(fmt.Sprintf("Failed synchronising peers for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			peerSyncAndIdentity.PeersSync = "?/?"
		}

		// Handle own identity
		_, err = r.publishIdentity(ctx, identity, ipfsClient)
		if err != nil {
			// It happens if we're alone on the ipfs network, and we try to publish something
			if ipfsclient.HasNoPeerError(err) {
				logger.Info(fmt.Sprintf("We are alone in swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
				peerSyncAndIdentity.PeersSync = "0/0"
			} else {
				logger.Info(fmt.Sprintf("Error publishing identity for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
				errs = append(errs, err)
			}
		} else {
			peerSyncAndIdentity.IdentitySync = clusterdisc.Synchronized
		}

		// Update given identity status
		identitiesStatus[swarmID.Namespace+"/"+swarmID.SwarmPoolName+"/"+swarmID.Swarm] = peerSyncAndIdentity
	}

	// Iterate over published identities in status, and check if some have been removed in current identity version
	// Not the canonic way of doing it, should introspect cluster state and all instead of relying on custom state saved in status,
	// but it doesn't make sense in this context

	for oldID := range identity.Status.IdentitiesStatus {
		found := false
		for newID := range identitiesStatus {
			if oldID == newID {
				found = true
			}
		}
		if !found {
			logger.Info("Old swarm found in status but not in current swarm " + oldID)
			// Need to delete the identity from the ipfs swarm
			// Instanciate IPFS client on swarmpool based on identities status key, not my proudest move
			chunks := strings.Split(oldID, "/")
			if err := r.Get(ctx, types.NamespacedName{Namespace: chunks[0], Name: chunks[1]}, &swarmPool); err == nil {
				// Retrieve targeted swarm
				for _, s := range swarmPool.Spec.Swarms {
					if s.Name == chunks[2] {
						swarm = s
						swarmNetwork = s.Network
						break
					}
				}

				// Build url of targeted service
				url, err := util.IPFSServiceURL(ctx, r, swarmPool, swarm)
				if err != nil {
					return ctrl.Result{}, err
				}

				// Remove identity from IPFS
				ipfsClient, err := ipfsclient.NewIPFSClient(*url)

				if err != nil {
					return ctrl.Result{}, err
				}

				if err := ipfsClient.DeleteIdentity(); err != nil {
					return ctrl.Result{}, err
				}
			} else {
				if apierrs.IsNotFound(err) {
					logger.Info("Old swarm not found")
				} else {
					logger.Error(err, "error retrieving swarmpool")
					return ctrl.Result{}, err
				}
			}

			// Remove namespace containing all peers related to deleted identity
			if err := r.Get(ctx, types.NamespacedName{Name: chunks[0] + "-" + chunks[1] + "-" + chunks[2]}, &namespace); err != nil {
				if apierrs.IsNotFound(err) {
					logger.Info("Namespace not found")
					continue
				} else {
					return ctrl.Result{}, err
				}
			}
			//TODO supprimer les peers associé
		}
	}

	//Update status, eventually
	if identity.Status.LastUpdate != "" {
		lastUpdate, _ = time.Parse(time.RFC3339, identity.Status.LastUpdate)
	}

	if identity.Status.LastUpdate == "" || time.Since(lastUpdate) > r.RefreshStatusPeriod {
		identity.Status.LastUpdate = time.Now().Format(time.RFC3339)
		identity.Status.IdentitiesStatus = identitiesStatus
		if err := r.Status().Update(ctx, &identity); err != nil {
			logger.Info("Error updating identity status", "swarm", swarm.Name)
			errs = append(errs, err)
		}
	}

	if len(errs) > 0 {
		for _, err := range errs {
			out.WriteString(err.Error() + "\n")
		}
		logger.Info("Identity processing finished with errors", "errors", out.String())
		return ctrl.Result{RequeueAfter: time.Duration(10 * time.Second)}, nil
	}
	return ctrl.Result{RequeueAfter: r.RefreshStatusPeriod}, nil
}

func (r *IdentityReconciler) publishIdentity(ctx context.Context, identity clusterdisc.Identity, ipfsClient *ipfsclient.IPFSClient) (*string, error) {

	var (
		identityStableName *string
		err                error
		identityBuf        bytes.Buffer
	)
	// Generate toml file from identity spec
	if err := toml.NewEncoder(&identityBuf).Encode(identity.Spec); err != nil {
		return nil, err
	}

	// Publish toml file to IPFS, will fail if alone in the swarm
	if identityStableName, err = ipfsClient.PublishIdentity(&identityBuf); err != nil {
		return nil, err
	}

	return identityStableName, nil
}

// Gather remote identities as Peer locally. Returns number or remote peers, number of error while publishing remote peers, and erros
// Returns nb peers, nb failed peers, and error
func (r *IdentityReconciler) syncPeers(ctx context.Context, identity clusterdisc.Identity, swarmPool clusterdisc.SwarmPool, ipfsClient *ipfsclient.IPFSClient) (*int, *int, error) {

	var (
		out           strings.Builder
		peerIds       []string
		nbPeers       int
		nbFailedPeers int
		existingPeers clusterdisc.PeerList
	)

	logger := log.FromContext(ctx).WithName(identity.Name)

	// Get current boostrap peers list
	bootstrapPeers, err := ipfsClient.GetBootstrapPeers(ctx)
	if err != nil {
		return nil, nil, err
	}

	// Force explicit connection to every bootstrap node
	for _, bootstrapPeer := range bootstrapPeers {
		// silently failed if can't connect to bootstrap node
		ipfsClient.ConnectTo(ctx, bootstrapPeer)
	}

	// Scan for connected peers
	peersAddr, err := ipfsClient.ListPeersAddress(ctx)
	if err != nil {
		return nil, nil, err
	}

	//Get out of here if no peers have been found
	if len(*peersAddr) == 0 {
		return nil, nil, err
	}

	// Extract peer Id from peer Address
	for _, addr := range *peersAddr {
		chunks := strings.Split(addr, "/")
		peerIds = append(peerIds, chunks[len(chunks)-1])
	}

	// Retrieve existing Peers instance in  given namespace
	if err := r.List(ctx, &existingPeers, &client.ListOptions{Namespace: swarmPool.Namespace}); err != nil {
		return nil, nil, err
	}

	for _, peer := range existingPeers.Items {
		foundPeer := false
		for _, peerID := range peerIds {
			if peer.Name == ipfsclient.ToDNS1123(peerID) {
				foundPeer = true
				break
			}
		}
		if !foundPeer {
			//Peer doesn't exist anymore in IPFS
			logger.Info("Peer doesn't exist anymore in ipfs network", "peer", peer.Spec.Name)
			peer.Status.MissingCounter++
			// Peer has been seen as missing more than 3 times, delete it
			if peer.Status.MissingCounter > 3 {
				if err := r.Delete(ctx, &peer); err != nil {
					return nil, nil, err
				}
				logger.Info("Peer deleted", "peer", peer.Spec.Name)
			} else {
				// Update counter
				if err := r.Status().Update(ctx, &peer); err != nil {
					return nil, nil, err
				}
			}
		} else {
			if peer.Status.MissingCounter != 0 {
				// Reinitialize counter
				peer.Status.MissingCounter = 0
				if err := r.Status().Update(ctx, &peer); err != nil {
					return nil, nil, err
				}
			}
		}
	}

	// Update bootstrap list
	if err := ipfsClient.UpdateBootstrapList(ctx, *peersAddr); err != nil {
		return nil, nil, err
	}

	// Creates or update each peer
	// Publish concurrently each peers in local kub instance with a worker pool

	errs := r.publishPeers(ctx, identity, swarmPool, peerIds, ipfsClient)

	// Concatenate errors, eventually
	if len(errs) > 0 {
		for _, err := range errs {
			if err != nil {
				out.WriteString(err.Error() + "\n")
			}
		}
		err = errors.New(out.String())
	} else {
		err = nil
	}

	nbPeers = len(*peersAddr)
	nbFailedPeers = len(errs)

	return &nbPeers, &nbFailedPeers, err
}

// publishPeers manages the concurrency via chan and go routine
func (r *IdentityReconciler) publishPeers(ctx context.Context, identity clusterdisc.Identity, swarmPool clusterdisc.SwarmPool, peersAddr []string, ipfsClient *ipfsclient.IPFSClient) []error {
	concurrency := 10
	in := make(chan string)
	out := make(chan error)
	errs := []error{}
	retVals := []error{}

	// Creates pool
	for i := 0; i < concurrency; i++ {
		go r.processPeer(ctx, identity, swarmPool, ipfsClient, in, out)
	}

	// Feed input
	go func() {
		for _, peer := range peersAddr {
			in <- peer
		}
		close(in)
	}()

	// Wait for output
	for val := range out {
		retVals = append(retVals, val)
		if len(retVals) == len(peersAddr) {
			break
		}
	}

	// Filter out nil error
	for _, retVal := range retVals {
		if retVal != nil {
			errs = append(errs, retVal)
		}
	}

	return errs
}

// processPeer get data from IPFS, and instanciate a Peer object
func (r *IdentityReconciler) processPeer(ctx context.Context, identity clusterdisc.Identity, swarmPool clusterdisc.SwarmPool, ipfsClient *ipfsclient.IPFSClient, in <-chan string, out chan<- error) {

	logger := log.FromContext(ctx).WithName(identity.Name)

	var (
		peer           clusterdisc.Peer
		peersNamespace corev1.Namespace
	)

	isController := true

	for peerAddr := range in {
		// Created from scratch based on informations published on ipfs network
		generatedPeer, err := ipfsClient.GetPeerIdentity(peerAddr)
		if err != nil {
			out <- err
			return
		}

		// Peers are generated in Swarmpool namespace
		generatedPeer.Namespace = swarmPool.Namespace

		// Create namespace, eventually
		if err := r.Get(ctx, types.NamespacedName{Name: generatedPeer.Namespace}, &peersNamespace); err != nil {
			// Peer doesn't exists ? Create it then
			if apierrs.IsNotFound(err) {
				peersNamespace.Name = generatedPeer.Namespace
				peersNamespace.OwnerReferences = []metav1.OwnerReference{{
					APIVersion: swarmPool.APIVersion,
					Kind:       swarmPool.Kind,
					Name:       swarmPool.Name,
					UID:        swarmPool.UID,
					Controller: &isController,
				}}
				//Does Creation fail ?
				if err := r.Create(ctx, &peersNamespace); err != nil {
					out <- err
					return
				}
			} else { // GET request failed for whatever reason
				out <- err
				return
			}
		}

		// Check if underlying peer cid has been "deleted" (ipns point to empty file, no info available), if so, delete the kub object
		if generatedPeer.Spec.URL == "" && generatedPeer.Spec.Description == "" && len(generatedPeer.Spec.Categories) == 0 {
			if err := r.Get(ctx, types.NamespacedName{Name: generatedPeer.Name, Namespace: generatedPeer.Namespace}, &peer); err == nil {
				if err := r.Delete(ctx, generatedPeer); err != nil {
					out <- err
				}
			}
			out <- errors.New(generatedPeer.Name + " Peer didn't publish identity")
			return
		}

		// If remote identity (aka Peer) exists, attach the peer to the current identity for garbage collector matters
		generatedPeer.OwnerReferences = []metav1.OwnerReference{{
			APIVersion: swarmPool.APIVersion,
			Kind:       swarmPool.Kind,
			Name:       swarmPool.Name,
			UID:        swarmPool.UID,
			Controller: &isController,
		}}

		//Create object, check if it already exists first
		if err := r.Get(ctx, types.NamespacedName{Name: generatedPeer.Name, Namespace: generatedPeer.Namespace}, &peer); err != nil {
			// Peer doesn't exists ? Create it then
			if apierrs.IsNotFound(err) {
				//Does Creation fail ?
				if err := r.Create(ctx, generatedPeer); err != nil {
					out <- err
				} else {
					logger.Info("Found a new peer !", "peer", generatedPeer.Spec.Name)
					out <- nil
				}
			} else { // GET request failed for whatever reason
				out <- err
			}
		} else {
			//

			// Update Peer kubernetes object
			peer.Spec.Name = generatedPeer.Spec.Name
			peer.Spec.Description = generatedPeer.Spec.Description
			peer.Spec.Categories = generatedPeer.Spec.Categories
			peer.Spec.URL = generatedPeer.Spec.URL

			if err := r.Update(ctx, &peer); err != nil {
				out <- err
			} else {
				out <- nil
			}
		}
	}
}

func (r *IdentityReconciler) deleteIdentity(ctx context.Context, identity *clusterdisc.Identity) []error {
	logger := log.FromContext(ctx).WithName(identity.Spec.Name)
	var (
		swarmPool    clusterdisc.SwarmPool
		swarm        clusterdisc.Swarm
		swarmNetwork clusterdisc.SwarmNetwork
		err          error
	)
	logger.Info("Emptying swarmpools associated with identity")
	errs := []error{}

	// Time to remove identity file from all swarmpools
	// Iterate over targeted swarms
	for _, swarmID := range identity.Spec.PublicationTargets {

		// Get the associated swarm pool
		err = r.Get(ctx, types.NamespacedName{Namespace: swarmID.Namespace, Name: swarmID.SwarmPoolName}, &swarmPool)

		// Skip if swarm pool doesn't exist, we can't do anything to resolve potential weird inconsistency at this stage
		if err != nil {

			logger.Info(fmt.Sprintf("Error retrieving swarm %s/%s associated with identity", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		// Retrieve targeted swarm
		for _, s := range swarmPool.Spec.Swarms {
			if s.Name == swarmID.Swarm {
				swarm = s
				swarmNetwork = s.Network
				break
			}
		}

		// check if swarm has been found
		if swarmNetwork.SwarmPort == 0 && swarmNetwork.APIPort == 0 {
			logger.Info(fmt.Sprintf("Invalid port info for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, SwarmNotFound{Reason: fmt.Sprintf("%s/%s/%s not found", swarmID.Namespace, swarmID.SwarmPoolName, swarmID.Swarm)})
			continue
		}

		// Instanciate IPFS client, get headless or lb url first
		url, err := util.IPFSServiceURL(ctx, r, swarmPool, swarm)
		if err != nil {
			logger.Info(fmt.Sprintf("Error generating IPFS service URL for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		ipfsClient, err := ipfsclient.NewIPFSClient(*url)
		if err != nil {
			logger.Info(fmt.Sprintf("Error generating IPFS client for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
			continue
		}

		if err := ipfsClient.DeleteIdentity(); err != nil {
			logger.Info(fmt.Sprintf("Error removin identity from IPFS client network for swarm %s/%s", swarmID.SwarmPoolName, swarmID.Swarm))
			errs = append(errs, err)
		}

	}
	return errs
}

func (r *IdentityReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&clusterdisc.Identity{}).
		Complete(r)
}
