package util

import (
	"context"
	"fmt"
	"net/url"
	"time"

	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	rm "irt.saint-exupery.com/cluster-discovery/resources_manager"

	corev1 "k8s.io/api/core/v1"
	apierrs "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func IgnoreNotFound(err error) error {
	if apierrs.IsNotFound(err) {
		return nil
	}
	return err
}

func IgnoreAlreadyExists(err error) error {
	if apierrs.IsAlreadyExists(err) {
		return nil
	}
	return err
}

func ContainsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func RemoveString(slice []string, s string) (result []string) {
	for _, item := range slice {
		if item == s {
			continue
		}
		result = append(result, item)
	}
	return
}

func IPFSServiceURL(ctx context.Context, r client.Reader, swarmPool cd.SwarmPool, swarm cd.Swarm) (*url.URL, error) {

	var svc corev1.Service
	var url *url.URL
	var err error

	rn := rm.NewResourcesNaming(swarmPool, swarm)

	// In this case, API is reachable thanks to load balancer
	if swarm.Network.LoadBalancer.ExposedAPI {
		if err := r.Get(ctx, rn.GetLoadBalancerNamespacedName(), &svc); err != nil {
			return nil, err
		}

		if len(svc.Status.LoadBalancer.Ingress) == 0 {
			return nil, fmt.Errorf("%s load balancer doesn't have an external IP address yet", rn.GetLoadBalancerNamespacedName())
		}

		IP := svc.Status.LoadBalancer.Ingress[0].IP
		port := swarm.Network.APIPort

		url, err = url.Parse(fmt.Sprintf("http://%s:%d", IP, port))
		if err != nil {
			panic(fmt.Sprintf("Unexpected error: %s", err))
		}
	} else {

		// Otherwise, requests are performed through headless service
		if err := r.Get(ctx, rn.GetIPFSAPIServiceNamespacedName(), &svc); err != nil {
			return nil, err
		}

		url, err = url.Parse(fmt.Sprintf("http://%s.%s:%d", rn.GetIPFSAPIServiceNamespacedName().Name,
			rn.GetIPFSAPIServiceNamespacedName().Namespace, swarm.Network.APIPort))
		if err != nil {
			panic(fmt.Sprintf("Unexpected error: %s", err))
		}
	}

	return url, nil
}

// Print error to standard output and requeue after 10 secondes
func ResultRequeueOnError(err error) (ctrl.Result, error) {
	if err != nil {
		fmt.Println(err)
		return ctrl.Result{RequeueAfter: time.Duration(10 * time.Second)}, nil
	}
	return ctrl.Result{}, nil
}
