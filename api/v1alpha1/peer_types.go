/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Connectivity value contains Peer status (online/offline)
type ConnectivityValues string

// Connectivity states.
const (
	Unknown ConnectivityValues = "Unknown"
	Online  ConnectivityValues = "Online"
	Offline ConnectivityValues = "Offline"
)

// PeerSpec defines the desired state of Peer
type PeerSpec struct {
	URL         string     `json:"url"`
	Description string     `json:"description"`
	Categories  Categories `json:"categories"`
	Name        string     `json:"name"`
}

// PeerStatus defines the observed state of Peer
type PeerStatus struct {
	PeerStatus ConnectivityValues `json:"status"`
	// count how many time the peer disapeared from underlying IPFS table
	MissingCounter int `json:"missingCounter"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope=Namespaced
// +kubebuilder:printcolumn:name="PeerName",type=string,JSONPath=`.spec.name`
// +kubebuilder:printcolumn:name="Status",type=string,JSONPath=`.status.status`
// +kubebuilder:printcolumn:name="URL",type=string,JSONPath=`.spec.url`
// +kubebuilder:printcolumn:name="Description",type=string,JSONPath=`.spec.description`
// Peer is the Schema for the peers API
type Peer struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PeerSpec   `json:"spec,omitempty"`
	Status PeerStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// PeerList contains a list of Peer
type PeerList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Peer `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Peer{}, &PeerList{})
}
