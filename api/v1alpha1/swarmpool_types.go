/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"reflect"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type SwarmStatus struct {
	Name                 string   `json:"Name"`
	ID                   string   `json:"Peer Id"`
	Addresses            []string `json:"Addresses"`
	NbConnectedPeers     int      `json:"Number of connected peers"`
	NbBootstrapAddresses int      `json:"Number of bootstraps"`
}

type TraefikIntegrationDefinition struct {
	TraefikEntryPoint            string `json:"traefikEntryPoint"`
	TraefikLoadBalancerService   string `json:"traefikLoadBalancerServiceName"`
	TraefikLoadBalancerNamespace string `json:"traefikLoadBalancerNamespace"`
}

type LoadBalancerDefinition struct {
	// Set value as "auto" to let the cloud manager provisioning an IP automatically
	LoadBalancerIP string `json:"loadBalancerIP"`
	ExposedAPI     bool   `json:"exposedAPI"`
}

type SwarmDefinition struct {
	BootstrapNodeAddresses []string `json:"bootstrapNodeAddresses,omitempty"`
	SwarmKeySecretRef      string   `json:"swarmKeySecretRef"`
}

type SwarmNetwork struct {
	Traefik      TraefikIntegrationDefinition `json:"traefik,omitempty"`
	LoadBalancer LoadBalancerDefinition       `json:"loadBalancer,omitempty"`
	SwarmPort    int                          `json:"swarmPort"`
	APIPort      int                          `json:"apiPort"`
	// If the load balancer doesn't have a public facing IP, no need to specify this
	// Controller will extract the load balancer IP and swarm Port from Load balancer configuration
	// to setup the underlying IPFS Node
	AnnouncePort int    `json:"announcePort,omitempty"`
	AnnounceIP   string `json:"announceIP,omitempty"`
}

type SwarmStorage struct {
	StorageSize string `json:"capacity"`
}

type Swarm struct {
	Name            string          `json:"name"`
	SwarmDefinition SwarmDefinition `json:"swarmDefinition,omitempty"`
	Network         SwarmNetwork    `json:"network"`
	Storage         SwarmStorage    `json:"storage"`
	IPFSVersion     string          `json:"IPFSVersion"`
}

// SwarmPoolSpec defines the desired state of SwarmPool
type SwarmPoolSpec struct {
	Swarms []Swarm `json:"swarms"`
}

// SwarmPoolStatus defines the observed state of SwarmPool
type SwarmPoolStatus struct {
	SwarmsStatus map[string]SwarmStatus `json:"status"`
	LastUpdate   string                 `json:"lastUpdate"`
	SpecHash     string                 `json:"hash"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:scope=Namespaced
// SwarmPool is the Schema for the swarmpools API
type SwarmPool struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SwarmPoolSpec   `json:"spec,omitempty"`
	Status SwarmPoolStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// SwarmPoolList contains a list of SwarmPool
type SwarmPoolList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SwarmPool `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SwarmPool{}, &SwarmPoolList{})
}

// HasSpecChanged returns true if object spec has changed
func (sp *SwarmPool) HasSpecChanged() bool {
	currentSpecHash := sp.specb64Hash()
	return sp.Status.SpecHash != currentSpecHash
}

// UpdateSpecHash computes and update spec hash, and stores it in status object field
func (sp *SwarmPool) UpdateSpecHash() {
	currentSpecHash := sp.specb64Hash()
	if sp.Status.SpecHash != currentSpecHash {
		sp.Status.SpecHash = currentSpecHash
	}
}

func (sp *SwarmPool) specb64Hash() string {
	toHash := ""
	e := reflect.ValueOf(&sp.Spec).Elem()

	for i := 0; i < e.NumField(); i++ {
		name := e.Type().Field(i).Name
		varType := e.Type().Field(i).Type
		value := e.Field(i).Interface()
		toHash = fmt.Sprintf("%v %v %v %v ", toHash, name, varType, value)
	}
	md5 := md5.Sum([]byte(toHash))
	return base64.StdEncoding.EncodeToString(md5[:])
}
