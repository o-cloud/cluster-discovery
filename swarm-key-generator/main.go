package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/akamensky/argparse"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	apierrs "k8s.io/apimachinery/pkg/api/errors"
)

/*
	Shamelessy -mostly- stolen from https://github.com/Kubuxu/go-ipfs-swarm-key-gen/blob/master/ipfs-swarm-key-gen/main.go
	Adding optional ConfigMap integration because hell yeah
*/

func main() {

	parser := argparse.NewParser("swarm_key_generator", "Generate a swarm key for IPFS private network")
	cmNamespace := parser.String("p", "cfg-namespace", &argparse.Options{Required: false, Help: "ConfigMap namespace to use"})
	cmName := parser.String("n", "cfg-name", &argparse.Options{Required: false, Help: "ConfigMap name to use"})
	swarmKeyName := parser.String("s", "swarmkey-name", &argparse.Options{Required: false, Help: "Swarm key name"})
	var configMap *corev1.ConfigMap
	var config *rest.Config
	alreadyExists := true

	//Handle args
	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}

	//Gen key
	key := make([]byte, 32)
	_, err = rand.Read(key)
	if err != nil {
		log.Fatalln("While trying to read random source:", err)
	}

	//Format key
	strKey := fmt.Sprintf("/key/swarm/psk/1.0.0/\n/base16/\n%s", hex.EncodeToString(key))
	fmt.Println("###########################################")
	fmt.Println("Generated key:")
	fmt.Println(strKey)
	fmt.Println("###########################################")
	fmt.Println("b64 encoded key:")
	fmt.Println(base64.StdEncoding.EncodeToString([]byte(strKey)))
	fmt.Println("###########################################")

	if *cmNamespace != "" && *cmName != "" && *swarmKeyName != "" {
		//retrieve k8s config
		config, err = rest.InClusterConfig()
		if err != nil {
			config, err = clientcmd.BuildConfigFromFlags("", filepath.Join(os.Getenv("HOME"), ".kube", "config"))
			if err != nil {
				panic(err.Error())
			}
		}

		// creates the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			panic(err.Error())
		}
		configMapClient := clientset.CoreV1().ConfigMaps(*cmNamespace)

		// Eventually, retrieve specified config map
		//TODO: Should be a secret here, and not a config map
		configMap, err = configMapClient.Get(*cmName, metav1.GetOptions{})
		if err != nil {
			if apierrs.IsNotFound(err) {
				alreadyExists = false
				configMap = &corev1.ConfigMap{
					ObjectMeta: metav1.ObjectMeta{
						Name:      *cmName,
						Namespace: *cmNamespace,
					},
					Data: make(map[string]string),
				}
			} else {
				panic(err.Error())
			}
		}

		configMap.Data[*swarmKeyName] = strKey
		configMap.Data["default"] = ""

		if alreadyExists {
			if _, err := configMapClient.Update(configMap); err != nil {
				panic(err.Error())
			}
		} else {
			if _, err := configMapClient.Create(configMap); err != nil {
				panic(err.Error())
			}
		}
	}
}
