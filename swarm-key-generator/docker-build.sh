#!/usr/bin/env bash

IMAGE_NAME=swarm-key-generator
VERSION=0.0.1
GCP_PROJECT_ID=irt-sb

docker build -t $IMAGE_NAME:$VERSION .
if [[ $# -eq 1 ]]; then
    if [[ "$1" == "--gcp-publish" ]]; then
        echo "Pushing image to docker registry"
        docker tag $IMAGE_NAME:$VERSION eu.gcr.io/$GCP_PROJECT_ID/$IMAGE_NAME:$VERSION
        # Must execute gcloud auth configure-docker 
        docker push eu.gcr.io/$GCP_PROJECT_ID/$IMAGE_NAME:$VERSION
    fi
fi

