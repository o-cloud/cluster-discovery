module swarm_key_generator

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/akamensky/argparse v1.2.0
	github.com/ipfs/go-ipfs-api v0.0.3
	k8s.io/api v0.17.0
	k8s.io/apimachinery v0.17.0
	k8s.io/client-go v0.17.0
)
