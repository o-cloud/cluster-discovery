#!/bin/bash

# Common data available in all network members to implement a data provider based peer discovery
printf "8edc7f45689294823d0b130063b0bb4b038bf54f" | ipfs add -

# Updating Address, compute.instances.get permission MUST be set to the role associated to the service
# account used by the compute engine to make it works
IP=$(curl http://metadata.google.internal/computeMetadata/v1/instance/attributes/publicIP -H "Metadata-Flavor: Google")
PORT=$(curl http://metadata.google.internal/computeMetadata/v1/instance/attributes/swarmPort -H "Metadata-Flavor: Google")
ADDRESS=/ip4/${IP}/tcp/${PORT}/

cp /home/ipfs/.ipfs/config /home/ipfs/.ipfs/config.bak
rm -f /home/ipfs/.ipfs/config
cat /home/ipfs/.ipfs/config.bak | jq '.Addresses.Announce[0] |= . + $ADDRESS' --arg ADDRESS $ADDRESS > /home/ipfs/.ipfs/config
chown ipfs:ipfs /home/ipfs/.ipfs/config

ipfs daemon
