resource "google_compute_instance_template" "ifps_bootstrap" {
  name        = "ifps-bootstrap-0-8-0"
  description = "IPFS bootstrap node template"

  instance_description = "IPFS 0.8.0 SB private swarm bootstrap node"
  machine_type         = var.machine_type
  can_ip_forward       = false

  scheduling {
    automatic_restart   = false
    preemptible = true
  }

    tags = ["ipfs-bootstrap-0-8-0"]

  disk {
    source_image = "ipfs-0-8-0"
    auto_delete  = true
    boot         = true
    disk_size_gb = var.disk_size
  }

  network_interface {
    network = google_compute_network.ipfs-bootstrap-network.name
    access_config {
      nat_ip = google_compute_address.static_ip.address
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-rw", "storage-ro"]
  }

  metadata = {
    publicIP = google_compute_address.static_ip.address
    swarmPort = var.swarm_port
  }

}



resource "google_compute_instance_group_manager" "ipfs_bootstrap_manager" {
  name               = "ipfs-bootstrap-manager-0-8-0"
  version {
    instance_template  = google_compute_instance_template.ifps_bootstrap.self_link
  }

  base_instance_name = "ifps-0-8-0"
  zone               = var.zone
  target_size        = 1
}