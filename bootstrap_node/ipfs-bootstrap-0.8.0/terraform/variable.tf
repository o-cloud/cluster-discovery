variable "region" {
  type = string
  default = "europe-west1"
}

variable "zone" {
  type = string
  default = "europe-west1-d"
}

variable "project_id" {
  type = string
  default = ""
}

variable "gcp_credentials" {
  type = string
  default = "/dev/null"
}

variable "disk_size" {
  type = number
  default = 11
}

variable "machine_type" {
  type = string
  default = "g1-small"
}

variable "swarm_port" {
  type = number
  default = 4001
}

variable "api_port" {
  type = number
  default = 5001
}