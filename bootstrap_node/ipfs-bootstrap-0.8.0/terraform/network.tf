resource "google_compute_address" "static_ip" {
  name   = "bootstrap-node-ip-0-8-0"
  region = var.region
}

output "bootstrap-node-ip" {
  value = google_compute_address.static_ip.address
}

resource "google_compute_firewall" "ifps-swarm" {
  name    = "ifps-swarm-0-8-0"
  network = google_compute_network.ipfs-bootstrap-network.name

  allow {
    protocol = "tcp"
    ports    = ["${var.swarm_port}"]
  }

  target_tags = ["ipfs-bootstrap-0-8-0"]
}

resource "google_compute_firewall" "ifps-api" {
  name    = "ifps-api-0-8-0"
  network = google_compute_network.ipfs-bootstrap-network.name

  allow {
    protocol = "tcp"
    ports    = ["${var.api_port}"]
  }

  target_tags = ["ipfs-bootstrap-0-8-0"]
}

resource "google_compute_firewall" "ssh" {
  name    = "allow-ssh-0-8-0"
  network = google_compute_network.ipfs-bootstrap-network.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  target_tags = ["ipfs-bootstrap-0-8-0"]
}



resource "google_compute_network" "ipfs-bootstrap-network" {
  name = "ipfs-bootstrap-network-0-8-0"
}
