region = "europe-west1"
zone = "europe-west1-d"
project_id = "irt-sb"
gcp_credentials = "../conf/authent_terraform.json"
disk_size = 10
machine_type = "f1-micro"
