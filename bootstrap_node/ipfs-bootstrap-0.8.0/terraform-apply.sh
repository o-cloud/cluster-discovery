#!/usr/bin/env bash

terraform init terraform
terraform apply -var-file=terraform/values.auto.tfvars $* terraform
