package ipfsclient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	identity "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"
	util "irt.saint-exupery.com/cluster-discovery/util"

	shell "github.com/ipfs/go-ipfs-api"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var sharedData = "8edc7f45689294823d0b130063b0bb4b038bf54f"

type Response struct {
	Addrs []string
	ID    string
}

type ProviderInfos struct {
	Extra     string
	ID        string
	Responses []Response
	Type      uint
}

/*
type MyShell struct {
	shell.Shell
}


// Should be the proper way to implement find providers method
// However, API request returns values with this format
// {...}
// {...}
// {...}
// Top level object is neither a list or a json object, therefore JSON decoding
// Only consider first element :/

func (shell *MyShell) FindProvs(key string, numProviders uint) (*ProvidersInfos, error) {

	p := &ProvidersInfos{}
	// Can't do this as output of request is not a valid JSON document
	//err := shell.Request("dht/findprovs", key).Option("num-providers", numProviders).Exec(context.Background(), p)
	return p, err
}
*/

type IPFSClient struct {
	URL       url.URL
	api       *shell.Shell
	sharedCID string
}

type BootstrapPeer struct {
	Peers []string
}

// NewIPFSClient is, basically, a constructor
func NewIPFSClient(url url.URL) (*IPFSClient, error) {
	client := &IPFSClient{URL: url}

	//client.api = &MyShell{*shell.NewShell(url.String())}
	client.api = shell.NewShell(url.String())

	if err := client.publishSharedData(); err != nil {
		return nil, err
	}

	return client, nil
}

// ID returns ... the peer id, and the addresses that might be used to connect to the node
func (ipfsc *IPFSClient) ID() (string, []string, error) {
	var chosenAddresses []string
	IDOutput, err := ipfsc.api.ID()
	if IDOutput == nil {
		return "", []string{}, err
	}
	for _, address := range IDOutput.Addresses {
		if strings.Contains(address, "ip4") && !strings.Contains(address, "127.0.0.1") {
			chosenAddresses = append(chosenAddresses, address)
		}
	}
	// Remove loopback address, and ipv6 address, keep the last one
	if err != nil {
		return "", []string{}, err
	}
	return IDOutput.ID, chosenAddresses, nil
}

// PublishIdentity publish buf content to DHT, and generate IPNS mutable entry
func (ipfsc *IPFSClient) PublishIdentity(buf *bytes.Buffer) (*string, error) {

	var (
		resp *shell.PublishResponse
		cid  string
		err  error
	)

	// Publish buffer content
	cid, err = ipfsc.api.Add(strings.NewReader(buf.String()))
	if err != nil {
		return nil, err
	}

	// Generate IPNS stable name for cid
	// Will fail if Peer is alone in its swarm https://github.com/ipfs/go-ipfs/issues/1941
	if resp, err = ipfsc.api.PublishWithDetails(cid, "self", 0, 0, true); err != nil {
		return nil, errors.New(err.Error())
	}

	return &resp.Name, nil
}

// Publish data common to all IPFS node. It'll later be used to find all providers of this data,
// and thus discover all the peers belonging to the swarm
func (ipfsc *IPFSClient) publishSharedData() error {
	if ipfsc.sharedCID == "" {
		cid, err := ipfsc.api.Add(bytes.NewBufferString(sharedData))
		if err != nil {
			return err
		}
		ipfsc.sharedCID = cid
	}
	return nil
}

// DeleteIdentity publish empty buf content to DHT, and make IPNS mutable entry points to this empty file
func (ipfsc *IPFSClient) DeleteIdentity() error {

	output := make(chan error, 1)

	go func(output chan error) {
		// Generate empty buffer, make ipns address point on this empty file
		buf := new(bytes.Buffer)
		buf.WriteByte('\n')
		cid, err := ipfsc.api.Add(strings.NewReader(buf.String()))
		if err != nil {
			output <- err
			return
		}

		if _, err := ipfsc.api.PublishWithDetails(cid, "self", 0, 0, true); err != nil {
			output <- err
			return
		}

		output <- nil

	}(output)

	select {
	case result := <-output:
		return result
	case <-time.After(5 * time.Second):
		return fmt.Errorf("timeout while overriding data pointed by ipns address during identity deletion")
	}
}

func (ipfsc *IPFSClient) FindProvs(ctx context.Context) (*[]string, error) {
	fetchedPi := []ProviderInfos{}
	addresses := []string{}

	c := &http.Client{
		Transport: &http.Transport{
			Proxy:             http.ProxyFromEnvironment,
			DisableKeepAlives: true,
		},
	}

	apiURL := ipfsc.URL
	apiURL.Path = "/api/v0/dht/findprovs"

	req, err := http.NewRequest("POST", apiURL.String(), nil)
	if err != nil {
		return &[]string{}, err
	}

	qp := req.URL.Query()
	qp.Add("arg", ipfsc.sharedCID)
	qp.Add("verbose", "false")
	qp.Add("num-providers", "1000")

	req.URL.RawQuery = qp.Encode()
	req.Close = true

	resp, err := c.Do(req)
	if err != nil {
		return &[]string{}, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &[]string{}, err
	}

	// Fix API output by turning it to a proper list
	fixedOutput := strings.Replace(string(body), "\n", ",", -1)
	fixedOutput = strings.TrimSuffix(fixedOutput, ",")
	fixedOutput = fmt.Sprintf("[%s]", fixedOutput)
	err = json.Unmarshal([]byte(fixedOutput), &fetchedPi)
	if err != nil {
		return &[]string{}, err
	}

	// Filter out empty responses, and format peer addresses
	for _, prov := range fetchedPi {
		// Only keep Type 4 https://github.com/ipfs/js-ipfs/issues/2868
		if prov.Type == 4 && prov.Responses != nil && len(prov.Responses) != 0 {
			for _, response := range prov.Responses {
				for _, addr := range response.Addrs {
					// Extract IP
					values := strings.Split(addr, "/")
					// Check IPV4 IP validity
					if len(values) >= 3 {
						IP := net.ParseIP(values[2])
						if IP != nil && !util.IsPrivateIP(IP) {
							addresses = append(addresses, addr+"/p2p/"+response.ID)
						}
					}
				}
			}
		}
	}

	return &addresses, nil
}

//ListPeersAddress returns known peers in the network, based on providers of a given key
func (ipfsc *IPFSClient) ListPeersAddress(ctx context.Context) (*[]string, error) {

	connectedPeers := []string{}
	peers, err := ipfsc.FindProvs(ctx)
	if err != nil {
		return &[]string{}, err
	}

	//Try to get connected, filter out the disconnected one
	for _, peer := range *peers {
		err := ipfsc.api.SwarmConnect(ctx, peer)
		if err == nil {
			connectedPeers = append(connectedPeers, peer)
		}
	}

	return &connectedPeers, nil
}

//GetBootstrapPeers returns peers registered as bootstrap
func (ipfsc *IPFSClient) GetBootstrapPeers(ctx context.Context) ([]string, error) {
	var obj BootstrapPeer
	if err := ipfsc.api.Request("bootstrap").Exec(context.Background(), &obj); err != nil {
		return []string{}, err
	}
	return obj.Peers, nil
}

func (ipfsc *IPFSClient) ConnectTo(ctx context.Context, url string) error {

	if err := ipfsc.api.SwarmConnect(ctx, url); err != nil {
		return err
	}
	return nil
}

//UpdateBootstrapList update known bootstrap peers if the amount of new peers is bigger than the already known one
func (ipfsc *IPFSClient) UpdateBootstrapList(ctx context.Context, peers []string) error {
	//Should only keep 20 nodes as bootstrap nodes
	// TODO: bootstrap peer list should be curated
	bootPeers, err := ipfsc.GetBootstrapPeers(ctx)
	if err != nil {
		return err
	}
	if len(bootPeers) < len(peers) {
		_, err = ipfsc.api.BootstrapRmAll()
		if err == nil {
			_, err = ipfsc.api.BootstrapAdd(peers)
			return err
		}
	}
	return err

}

//GetPeerIdentity build a Peer Kubernetes object based on given peer Id
func (ipfsc *IPFSClient) GetPeerIdentity(PeerId string) (*identity.Peer, error) {

	var (
		identitySpec *identity.IdentitySpec
	)

	type Output struct {
		identitySpec *identity.IdentitySpec
		err          error
	}

	outChan := make(chan *Output, 1)

	go func(output chan *Output) {
		outputData := &Output{
			identitySpec: nil,
			err:          nil,
		}

		reader, err := ipfsc.api.Cat("/ipns/" + PeerId)
		if err != nil {
			outputData.err = fmt.Errorf("%s: %s", PeerId, err.Error())
			output <- outputData
			return
		}

		// Split ToML into two seperate file to unserialize in Peer and Kubeconfig instance
		buf := bytes.NewBuffer([]byte{})
		if _, err := buf.ReadFrom(reader); err != nil {
			outputData.err = err
			output <- outputData
			return
		}

		tomls := strings.Split(buf.String(), "#Kubeconfig")

		// Handling identity part
		outputData.identitySpec = &identity.IdentitySpec{}
		if len(tomls) > 0 {
			_, err = toml.Decode(tomls[0], outputData.identitySpec)
			if err != nil {
				outputData.identitySpec = nil
				outputData.err = fmt.Errorf("%s: %s", PeerId, err.Error())
				output <- outputData
				return
			}
		}
		output <- outputData
	}(outChan)

	select {
	case res := <-outChan:
		if res.err != nil {
			return nil, res.err
		}
		identitySpec = res.identitySpec

	case <-time.After(30 * time.Second):
		return nil, fmt.Errorf("%s: timeout fetching identity data from ipfs", PeerId)
	}

	return &identity.Peer{
		ObjectMeta: metav1.ObjectMeta{
			Name: ToDNS1123(PeerId),
		},

		Spec: identity.PeerSpec{
			URL:         identitySpec.URL,
			Description: identitySpec.Description,
			Categories:  identitySpec.Categories,
			Name:        identitySpec.Name,
		},
		Status: identity.PeerStatus{},
	}, nil
}

//ToDNS1123 turns a given string to a valid DNS1123 name format
func ToDNS1123(val string) string {
	return strings.ToLower(strings.Replace(strings.Replace(val, ".", "-", -1), ":", "-", -1))
}

//HasNoPeerError introspects error and checks for specific "no peer in table" IPFS error (it happens if the IPFS node is alone in its swarm)
func HasNoPeerError(err error) bool {
	return err.Error() == "name/publish: failed to find any peer in table"
}
