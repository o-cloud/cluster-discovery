package resourcesmanager

import (
	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	"k8s.io/apimachinery/pkg/types"
)

type ResourcesNaming struct {
	swarmPool cd.SwarmPool
	swarm     cd.Swarm
}

func NewResourcesNaming(swarmpool cd.SwarmPool, swarm cd.Swarm) ResourcesNaming {
	return ResourcesNaming{
		swarmPool: swarmpool,
		swarm:     swarm,
	}
}

func (rn *ResourcesNaming) GetLoadBalancerNamespacedName() types.NamespacedName {
	return types.NamespacedName{Namespace: rn.swarmPool.Namespace,
		Name: rn.swarmPool.Name + "-" + rn.swarm.Name + "-lb"}
}

func (rn *ResourcesNaming) GetStatefulSetNamespacedName() types.NamespacedName {
	return types.NamespacedName{Namespace: rn.swarmPool.Namespace,
		Name: rn.swarmPool.Name + "-" + rn.swarm.Name + "-ss"}
}

func (rn *ResourcesNaming) GetIPFSAPIServiceNamespacedName() types.NamespacedName {
	return types.NamespacedName{Namespace: rn.swarmPool.Namespace,
		Name: rn.swarmPool.Name + "-" + rn.swarm.Name + "-cip"}
}

func (rn *ResourcesNaming) GetTraefikTCPServiceNamespacedName() types.NamespacedName {
	return types.NamespacedName{Namespace: rn.swarmPool.Namespace,
		Name: rn.swarmPool.Name + "-" + rn.swarm.Name + "-traefik-cip"}
}

func (rn *ResourcesNaming) GetTraefikIngressRouteNamespacedName() types.NamespacedName {
	return types.NamespacedName{Namespace: rn.swarmPool.Namespace,
		Name: rn.swarmPool.Name + "-" + rn.swarm.Name + "-ingress-route"}
}

func (rn *ResourcesNaming) GetSwarmName() string {
	return rn.swarm.Name
}

func (rn *ResourcesNaming) GetSwarmPoolName() string {
	return rn.swarmPool.Name
}
