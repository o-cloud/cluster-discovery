package resourcesmanager

import (
	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ClusterIPResourcesManager struct {
	resourceNaming ResourcesNaming
	ownerRef       metav1.OwnerReference
	swarm          cd.Swarm
}

func NewClusterIPResourcesManager(ownerRef metav1.OwnerReference, rn ResourcesNaming, swarm cd.Swarm) ClusterIPResourcesManager {
	return ClusterIPResourcesManager{
		resourceNaming: rn,
		ownerRef:       ownerRef,
		swarm:          swarm,
	}
}

func (crm *ClusterIPResourcesManager) ClusterIPManifest() *corev1.Service {

	selector := make(map[string]string)
	selector["app"] = crm.resourceNaming.GetStatefulSetNamespacedName().Name

	labels := make(map[string]string)
	labels["swarmPool"] = crm.resourceNaming.GetSwarmPoolName()
	labels["swarm"] = crm.resourceNaming.GetSwarmName()

	ports := []corev1.ServicePort{
		{
			Name:     "api",
			Protocol: "TCP",
			Port:     int32(crm.swarm.Network.APIPort),
		},
	}

	svc := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      crm.resourceNaming.GetIPFSAPIServiceNamespacedName().Name,
			Namespace: crm.resourceNaming.GetIPFSAPIServiceNamespacedName().Namespace,
			Labels:    labels,
			OwnerReferences: []metav1.OwnerReference{
				crm.ownerRef,
			},
		},

		Spec: corev1.ServiceSpec{
			Type:      "ClusterIP",
			Selector:  selector,
			ClusterIP: "None",
			Ports:     ports,
		},
	}

	return &svc
}

func (crm *ClusterIPResourcesManager) UpdateClusterIPManifest(svc corev1.Service) *corev1.Service {
	ports := []corev1.ServicePort{
		{
			Name:     "api",
			Protocol: "TCP",
			Port:     int32(crm.swarm.Network.APIPort),
		},
	}

	svc.Spec.Ports = ports

	return &svc
}
