package resourcesmanager

import (
	"fmt"
	"html/template"
	"strings"

	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var initTemplate = `
#!/bin/sh
set -x

# Only execute this init script on first execution
if [ ! -f /data/config ]; then 
    /usr/local/bin/ipfs init -p server

	rm -rf /data/swarm.key
	rm -rf /data/api

	# If custom swarm key is provided, clean bootstrap and link key path into ipfs workspace
	if [ -f /key/swarm.key ] && [ $(cat /key/swarm.key | wc -l) -ne 0 ]; then 
		ln -s /key/swarm.key /data/swarm.key
		/usr/local/bin/ipfs bootstrap rm --all
		ipfs bootstrap add {{.BootstrapAddresses}}
	fi

	# Set public address
	ANNOUNCE_ADDR="/ip4/{{.IP}}/tcp/{{.SwarmPort}}/"
	SWARM_ADDR="/ip4/0.0.0.0/tcp/{{.SwarmPort}}/"
	ipfs config --json Addresses.Announce "[\"$ANNOUNCE_ADDR\"]"
	ipfs config --json Addresses.Swarm "[\"$SWARM_ADDR\"]"

	# Expose API server
	API_ADDR="/ip4/0.0.0.0/tcp/{{.APIPort}}/"
	ipfs config Addresses.API $API_ADDR

	# Force server mode
	ipfs config Routing.Type dhtserver

	# Remove private network adress in filter
	FILTER_ADDRS="
		\"/ip4/100.64.0.0/ipcidr/10\",
		\"/ip4/169.254.0.0/ipcidr/16\",
		\"/ip4/192.0.0.0/ipcidr/24\",
		\"/ip4/192.0.2.0/ipcidr/24\",
		\"/ip4/198.18.0.0/ipcidr/15\",
		\"/ip4/198.51.100.0/ipcidr/24\",
		\"/ip4/203.0.113.0/ipcidr/24\",
		\"/ip4/240.0.0.0/ipcidr/4\",
		\"/ip6/100::/ipcidr/64\",
		\"/ip6/2001:db8::/ipcidr/32\",
		\"/ip6/fc00::/ipcidr/7\",
		\"/ip6/ff00::/ipcidr/8\",
		\"/ip6/fe80::/ipcidr/10\"
	"
	ipfs config --json Swarm.AddrFilters "[$FILTER_ADDRS]"

fi
exit 0
`

const (
	DEFAULT_IPFS_VERSION string = "v0.8.0"
	DEFAULT_IPFS_IMAGE   string = "docker.io/ipfs/go-ipfs"
)

type StatefulSetResourcesManager struct {
	resourceNaming   ResourcesNaming
	ownerRef         metav1.OwnerReference
	edgeIP           string
	swarm            cd.Swarm
	defaultBootstrap string
}

type IPFSInitConfig struct {
	IP                 string
	SwarmPort          int
	APIPort            int
	BootstrapAddresses string
}

func generateIPFSInitConfig(config IPFSInitConfig) string {
	tmpl, err := template.New("ipfsInitConfig").Parse(initTemplate)
	var output strings.Builder
	if err != nil {
		panic(err)
	}

	err = tmpl.Execute(&output, config)
	if err != nil {
		panic(err)
	}
	return output.String()
}

// NewEdgeResourcesManager is a EdgeResourcesManager Constructor
func NewStatefulSetResourcesManager(ownerRef metav1.OwnerReference, rn ResourcesNaming, swarm cd.Swarm, edgeIP string, defaultBootstrap string) StatefulSetResourcesManager {
	return StatefulSetResourcesManager{
		resourceNaming:   rn,
		ownerRef:         ownerRef,
		edgeIP:           edgeIP,
		swarm:            swarm,
		defaultBootstrap: defaultBootstrap,
	}
}

func (ssrm *StatefulSetResourcesManager) StatefulSetManifest() (*appsv1.StatefulSet, error) {
	nbReplicas := int32(1)
	terminationGracePeriodSeconds := int64(10)
	selector := metav1.LabelSelector{
		MatchLabels:      make(map[string]string),
		MatchExpressions: []metav1.LabelSelectorRequirement{},
	}
	stsLabels := make(map[string]string)
	podLabels := make(map[string]string)
	resources := make(map[corev1.ResourceName]resource.Quantity)
	val, err := resource.ParseQuantity(ssrm.swarm.Storage.StorageSize)
	if err != nil {
		return &appsv1.StatefulSet{}, err
	}
	resources["storage"] = val

	selector.MatchLabels["app"] = ssrm.resourceNaming.GetStatefulSetNamespacedName().Name
	podLabels["app"] = ssrm.resourceNaming.GetStatefulSetNamespacedName().Name
	stsLabels["swarmPool"] = ssrm.resourceNaming.GetSwarmPoolName()
	stsLabels["swarm"] = ssrm.resourceNaming.GetSwarmName()

	IPFSVersion := ssrm.swarm.IPFSVersion
	if IPFSVersion == "" {
		IPFSVersion = DEFAULT_IPFS_VERSION
	}

	// If no swarm definition is provided, use the default key and bootstrap value
	var swarmKeySecretRef string
	var bootstrapAddresses string

	if ssrm.swarm.SwarmDefinition.SwarmKeySecretRef == "" && ssrm.swarm.SwarmDefinition.BootstrapNodeAddresses == nil {
		swarmKeySecretRef = "default-swarm-key"
		bootstrapAddresses = ssrm.defaultBootstrap
	} else {
		swarmKeySecretRef = ssrm.swarm.SwarmDefinition.SwarmKeySecretRef
		bootstrapAddresses = strings.Join(ssrm.swarm.SwarmDefinition.BootstrapNodeAddresses, ",")
	}

	// If AnnouncePort or AnnounceIP are specified, use them instead of edgeIP and configured port value
	// Useful is the LoadBalancer service didn't get a public routable IP
	ipfsPort := ssrm.swarm.Network.AnnouncePort
	ipfsIP := ssrm.swarm.Network.AnnounceIP

	if ipfsPort == 0 {
		ipfsPort = ssrm.swarm.Network.SwarmPort
	}

	if ipfsIP == "" {
		ipfsIP = ssrm.edgeIP
	}

	ss := appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ssrm.resourceNaming.GetStatefulSetNamespacedName().Name,
			Namespace: ssrm.resourceNaming.GetStatefulSetNamespacedName().Namespace,
			Labels:    stsLabels,
			OwnerReferences: []metav1.OwnerReference{
				ssrm.ownerRef,
			},
		},

		Spec: appsv1.StatefulSetSpec{
			Replicas:    &nbReplicas,
			Selector:    &selector,
			ServiceName: ssrm.resourceNaming.GetIPFSAPIServiceNamespacedName().Name,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: podLabels,
				},
				Spec: corev1.PodSpec{
					TerminationGracePeriodSeconds: &terminationGracePeriodSeconds,
					InitContainers: []corev1.Container{
						{
							Name:            "ipfs-init",
							Image:           fmt.Sprintf("%s:%s", DEFAULT_IPFS_IMAGE, IPFSVersion),
							ImagePullPolicy: corev1.PullAlways,
							Command: []string{
								"/bin/sh",
							},
							Args: []string{
								"-c",
								generateIPFSInitConfig(IPFSInitConfig{
									IP:                 ipfsIP,
									APIPort:            ssrm.swarm.Network.APIPort,
									SwarmPort:          ipfsPort,
									BootstrapAddresses: bootstrapAddresses,
								}),
							},

							Env: []corev1.EnvVar{
								{
									Name:  "IPFS_PATH",
									Value: "/data",
								},
							},
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      ssrm.resourceNaming.GetStatefulSetNamespacedName().Name + "-data",
									MountPath: "/data",
								},
							},
						},
					},
					Containers: []corev1.Container{
						{
							Name:            "ipfs",
							Image:           fmt.Sprintf("%s:%s", DEFAULT_IPFS_IMAGE, IPFSVersion),
							ImagePullPolicy: corev1.PullAlways,
							Command: []string{
								"/usr/local/bin/ipfs",
							},
							Args: []string{
								"daemon",
								"--debug",
							},
							Ports: []corev1.ContainerPort{
								{
									Name:          "swarm",
									ContainerPort: int32(ssrm.swarm.Network.SwarmPort),
								},
								{
									Name:          "api",
									ContainerPort: int32(ssrm.swarm.Network.APIPort),
								},
							},
							Env: []corev1.EnvVar{
								{
									Name:  "IPFS_PATH",
									Value: "/data",
								},
								{
									Name:  "LIBP2P_FORCE_PNET",
									Value: "1",
								},
							},
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      ssrm.resourceNaming.GetStatefulSetNamespacedName().Name + "-data",
									MountPath: "/data",
								},
							},
						},
					},
					Volumes: []corev1.Volume{},
				},
			},
			VolumeClaimTemplates: []corev1.PersistentVolumeClaim{
				{
					ObjectMeta: metav1.ObjectMeta{
						Name: ssrm.resourceNaming.GetStatefulSetNamespacedName().Name + "-data",
					},
					Spec: corev1.PersistentVolumeClaimSpec{
						AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: resources,
						},
					},
				},
			},
		},
	}

	// Configure swarm key injection
	ss.Spec.Template.Spec.InitContainers[0].VolumeMounts = append(ss.Spec.Template.Spec.InitContainers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      "swarm-key",
			MountPath: "/key",
		})

	ss.Spec.Template.Spec.Containers[0].VolumeMounts = append(ss.Spec.Template.Spec.Containers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      "swarm-key",
			MountPath: "/key",
		})

	ss.Spec.Template.Spec.Volumes = append(ss.Spec.Template.Spec.Volumes, corev1.Volume{
		Name: "swarm-key",
		VolumeSource: corev1.VolumeSource{
			Secret: &corev1.SecretVolumeSource{
				SecretName: swarmKeySecretRef,
				Items: []corev1.KeyToPath{
					{
						Key:  "swarm-key",
						Path: "swarm.key",
					},
				},
			},
		},
	})
	return &ss, nil
}

func (ssrm *StatefulSetResourcesManager) UpdateStatefulSetManifest(ss *appsv1.StatefulSet) *appsv1.StatefulSet {

	// If no swarm definition is provided, use the default key and bootstrap value
	var swarmKeySecretRef string
	var bootstrapAddresses string

	if ssrm.swarm.SwarmDefinition.SwarmKeySecretRef == "" && ssrm.swarm.SwarmDefinition.BootstrapNodeAddresses == nil {
		swarmKeySecretRef = "default-swarm-key"
		bootstrapAddresses = ssrm.defaultBootstrap
	} else {
		swarmKeySecretRef = ssrm.swarm.SwarmDefinition.SwarmKeySecretRef
		bootstrapAddresses = strings.Join(ssrm.swarm.SwarmDefinition.BootstrapNodeAddresses, ",")
	}

	// If AnnouncePort or AnnounceIP are specified, use them instead of edgeIP and configured port value
	// Useful is the LoadBalancer service didn't get a public routable IP
	ipfsPort := ssrm.swarm.Network.AnnouncePort
	ipfsIP := ssrm.swarm.Network.AnnounceIP

	if ipfsPort == 0 {
		ipfsPort = ssrm.swarm.Network.SwarmPort
	}

	if ipfsIP == "" {
		ipfsIP = ssrm.edgeIP
	}

	ss.Spec.Template.Spec.InitContainers[0].Args = []string{
		"-c",
		generateIPFSInitConfig(IPFSInitConfig{
			IP:                 ipfsIP,
			APIPort:            ssrm.swarm.Network.APIPort,
			SwarmPort:          ipfsPort,
			BootstrapAddresses: bootstrapAddresses,
		})}

	ss.Spec.Template.Spec.Containers[0].Ports = []corev1.ContainerPort{
		{
			Name:          "swarm",
			ContainerPort: int32(ssrm.swarm.Network.SwarmPort),
		},
		{
			Name:          "api",
			ContainerPort: int32(ssrm.swarm.Network.APIPort),
		},
	}

	// Configure swarm key injection, if any
	ss.Spec.Template.Spec.InitContainers[0].VolumeMounts = []corev1.VolumeMount{}
	ss.Spec.Template.Spec.Containers[0].VolumeMounts = []corev1.VolumeMount{}

	ss.Spec.Template.Spec.InitContainers[0].VolumeMounts = append(ss.Spec.Template.Spec.InitContainers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      ss.Name + "-data",
			MountPath: "/data",
		})

	ss.Spec.Template.Spec.Containers[0].VolumeMounts = append(ss.Spec.Template.Spec.Containers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      ss.Name + "-data",
			MountPath: "/data",
		})

	ss.Spec.Template.Spec.InitContainers[0].VolumeMounts = append(ss.Spec.Template.Spec.InitContainers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      "swarm-key",
			MountPath: "/key",
		})

	ss.Spec.Template.Spec.Containers[0].VolumeMounts = append(ss.Spec.Template.Spec.Containers[0].VolumeMounts,
		corev1.VolumeMount{
			Name:      "swarm-key",
			MountPath: "/key",
		})

	//Check if swarm volume exists, if not create it
	found := false
	for _, volume := range ss.Spec.Template.Spec.Volumes {
		if volume.Name == "swarm-key" {
			found = true
			break
		}
	}
	if !found {
		ss.Spec.Template.Spec.Volumes = append(ss.Spec.Template.Spec.Volumes, corev1.Volume{
			Name: "swarm-key",
			VolumeSource: corev1.VolumeSource{
				Secret: &corev1.SecretVolumeSource{
					SecretName: swarmKeySecretRef,
					Items: []corev1.KeyToPath{
						{
							Key:  "swarm-key",
							Path: "swarm.key",
						},
					},
				},
			},
		})
	}

	return ss
}
