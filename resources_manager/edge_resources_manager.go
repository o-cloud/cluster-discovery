package resourcesmanager

import (
	"strings"

	cd "irt.saint-exupery.com/cluster-discovery/api/v1alpha1"

	traefik "github.com/traefik/traefik/v2/pkg/provider/kubernetes/crd/traefik/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

type EdgeResourcesManager struct {
	resourceNaming    ResourcesNaming
	edgeConfiguration cd.SwarmNetwork
	ownerRef          metav1.OwnerReference
	IP                string
}

// NewEdgeResourcesManager is a EdgeResourcesManager Constructor
func NewEdgeResourcesManager(ownerRef metav1.OwnerReference,
	rn ResourcesNaming,
	swarmNetwork cd.SwarmNetwork) EdgeResourcesManager {
	return EdgeResourcesManager{
		resourceNaming:    rn,
		edgeConfiguration: swarmNetwork,
		ownerRef:          ownerRef,
	}
}

func (erm *EdgeResourcesManager) HasLoadBalancer() bool {
	return erm.edgeConfiguration.LoadBalancer.LoadBalancerIP != ""
}

func (erm *EdgeResourcesManager) HasTraefikIntegration() bool {
	return erm.edgeConfiguration.Traefik.TraefikEntryPoint != ""
}

func (erm *EdgeResourcesManager) LoadBalancerManifest() *corev1.Service {

	selector := make(map[string]string)
	selector["statefulset.kubernetes.io/pod-name"] = erm.resourceNaming.GetStatefulSetNamespacedName().Name + "-0"

	labels := make(map[string]string)
	labels["swarmPool"] = erm.resourceNaming.GetSwarmPoolName()
	labels["swarm"] = erm.resourceNaming.GetSwarmName()

	ports := []corev1.ServicePort{
		{
			Name:       "swarm",
			Protocol:   "TCP",
			Port:       int32(erm.edgeConfiguration.SwarmPort),
			TargetPort: intstr.IntOrString{IntVal: int32(erm.edgeConfiguration.SwarmPort)},
		},
	}

	// Do we expose API port to via load balancer ?
	if erm.edgeConfiguration.LoadBalancer.ExposedAPI {
		ports = append(ports,
			corev1.ServicePort{
				Name:       "api",
				Protocol:   "TCP",
				Port:       int32(erm.edgeConfiguration.APIPort),
				TargetPort: intstr.IntOrString{IntVal: int32(erm.edgeConfiguration.APIPort)},
			},
		)
	}

	lbIPValue := erm.edgeConfiguration.LoadBalancer.LoadBalancerIP
	if strings.ToLower(lbIPValue) == "auto" {
		lbIPValue = ""
	}

	lb := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:            erm.resourceNaming.GetLoadBalancerNamespacedName().Name,
			Namespace:       erm.resourceNaming.GetLoadBalancerNamespacedName().Namespace,
			Labels:          labels,
			OwnerReferences: []metav1.OwnerReference{erm.ownerRef},
		},

		Spec: corev1.ServiceSpec{
			Type:                  "LoadBalancer",
			ExternalTrafficPolicy: "Local",
			Selector:              selector,
			LoadBalancerIP:        lbIPValue,
			Ports:                 ports,
		},
	}

	return &lb
}

func (erm *EdgeResourcesManager) UpdateLoadBalancerManifest(svc *corev1.Service, swarm cd.Swarm) *corev1.Service {
	// Update Ports
	ports := []corev1.ServicePort{
		{
			Name:       "swarm",
			Protocol:   "TCP",
			Port:       int32(swarm.Network.SwarmPort),
			TargetPort: intstr.IntOrString{IntVal: int32(swarm.Network.SwarmPort)},
		},
	}

	// Do we expose API port to via load balancer ?
	if swarm.Network.LoadBalancer.ExposedAPI {
		ports = append(ports,
			corev1.ServicePort{
				Name:       "api",
				Protocol:   "TCP",
				Port:       int32(swarm.Network.APIPort),
				TargetPort: intstr.IntOrString{IntVal: int32(swarm.Network.APIPort)},
			},
		)
	}

	svc.Spec.Ports = ports
	svc.Spec.LoadBalancerIP = swarm.Network.LoadBalancer.LoadBalancerIP

	return svc

}

func (erm *EdgeResourcesManager) IngressRouteManifest() *traefik.IngressRouteTCP {
	selector := make(map[string]string)
	selector["app"] = erm.resourceNaming.GetStatefulSetNamespacedName().Name

	labels := make(map[string]string)
	labels["swarmPool"] = erm.resourceNaming.GetSwarmPoolName()
	labels["swarm"] = erm.resourceNaming.GetSwarmName()

	terminationDelay := -1

	tcpRouteManifest := traefik.IngressRouteTCP{
		ObjectMeta: metav1.ObjectMeta{
			Name:            erm.resourceNaming.GetTraefikIngressRouteNamespacedName().Name,
			Namespace:       erm.resourceNaming.GetTraefikIngressRouteNamespacedName().Namespace,
			Labels:          labels,
			OwnerReferences: []metav1.OwnerReference{erm.ownerRef},
		},

		Spec: traefik.IngressRouteTCPSpec{
			EntryPoints: []string{erm.edgeConfiguration.Traefik.TraefikEntryPoint},
			Routes: []traefik.RouteTCP{
				{
					Match: "HostSNI(`*`)",
					Services: []traefik.ServiceTCP{
						{
							Name:             erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Name,
							Namespace:        erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Namespace,
							Port:             intstr.FromInt(erm.edgeConfiguration.SwarmPort),
							TerminationDelay: &terminationDelay,
						},
					},
				},
			},
		},
	}

	return &tcpRouteManifest
}

func (erm *EdgeResourcesManager) UpdateIngressRouteManifest(ingressRoute traefik.IngressRouteTCP) *traefik.IngressRouteTCP {
	tcpRoute := []traefik.RouteTCP{
		{
			Match: "HostSNI(`*`)",
			Services: []traefik.ServiceTCP{
				{
					Name:      erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Name,
					Namespace: erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Namespace,
					Port:      intstr.FromInt(erm.edgeConfiguration.SwarmPort),
				},
			},
		},
	}

	ingressRoute.Spec.Routes = tcpRoute
	return &ingressRoute
}

func (erm *EdgeResourcesManager) TraefikServiceManifest() *corev1.Service {
	selector := make(map[string]string)
	selector["app"] = erm.resourceNaming.GetStatefulSetNamespacedName().Name

	labels := make(map[string]string)
	labels["swarmPool"] = erm.resourceNaming.GetSwarmPoolName()
	labels["swarm"] = erm.resourceNaming.GetSwarmName()

	ports := []corev1.ServicePort{
		{
			Name:       "swarm",
			Protocol:   "TCP",
			Port:       int32(erm.edgeConfiguration.SwarmPort),
			TargetPort: intstr.IntOrString{IntVal: int32(erm.edgeConfiguration.SwarmPort)},
		},
	}

	svc := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:            erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Name,
			Namespace:       erm.resourceNaming.GetTraefikTCPServiceNamespacedName().Namespace,
			Labels:          labels,
			OwnerReferences: []metav1.OwnerReference{erm.ownerRef},
		},

		Spec: corev1.ServiceSpec{
			Type:      "ClusterIP",
			Selector:  selector,
			ClusterIP: "None",
			Ports:     ports,
		},
	}

	return &svc
}

func (erm *EdgeResourcesManager) UpdateTraefikServiceManifest(svc corev1.Service) *corev1.Service {
	ports := []corev1.ServicePort{
		{
			Name:     "api",
			Protocol: "TCP",
			Port:     int32(erm.edgeConfiguration.SwarmPort),
		},
	}

	svc.Spec.Ports = ports

	return &svc
}

func (erm *EdgeResourcesManager) GetIP() string {
	return erm.IP
}

func (erm *EdgeResourcesManager) SetIP(IP string) {
	erm.IP = IP
}
