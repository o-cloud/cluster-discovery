# Build the manager binary
FROM golang:1.16 as builder

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY api/ api/
COPY controllers/ controllers/
COPY ipfs_client/ ipfs_client/
COPY util/ util/
COPY errors/ errors/
COPY metrics/ metrics/
COPY resources_manager/ resources_manager/


# Pass skaffold debug when used (by default is empty)
ARG SKAFFOLD_GO_GCFLAGS

# Build
RUN CGO_ENABLED=0 GO111MODULE=on go build -gcflags="${SKAFFOLD_GO_GCFLAGS}" -a -o manager main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot 
WORKDIR /
COPY --from=builder /workspace/manager .
USER 65532:65532


ENV GOTRACEBACK=all

ENTRYPOINT ["/manager"]
